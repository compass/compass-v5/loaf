#include <catch2/catch_test_macros.hpp>

#include "loaf/block_matrix.h"
#include "loaf/factories.h"
// #include "loaf/matrix.h"
#include "loaf/solve.h"
#include "mesh.h"

using namespace loaf;

// template <size_type bs>
// void test_one_physics_matrix() {
//     auto diag_block = 2.0 * ElementaryBlock<bs, bs>::Identity();
//     auto non_diag_block = -1. * ElementaryBlock<bs, bs>::Identity();
//     auto&& [J, RHS] = make_linear_system<bs>(sites);

//     auto J_CC = J.extract(cells, cells);
//     auto J_NN = J.extract(nodes, nodes);
//     auto J_CN = J.extract(cells, nodes);
//     auto J_NC = J.extract(nodes, cells);
//     for (auto&& i : cells) {
//         J_CC(i, i) += diag_block;
//     }

//     for (auto&& i : nodes) {
//         J_NN(i, i) += diag_block;
//     }

//     for (auto&& [i, neighbours] : CN_con.targets_by_source()) {
//         for (auto&& j : neighbours) {
//             J_CN(i, j) += non_diag_block;
//         }
//     }

//     for (auto&& [i, neighbours] : NC_con.targets_by_source()) {
//         for (auto&& j : neighbours) {
//             J_NC(i, j) += non_diag_block;
//         }
//     }
//     auto&& [m, n] = J.size();
//     std::cout << "Scalar storage test:" << std::endl;
//     std::cout << "   "
//               << "Nodes, cells, block size" << std::endl;
//     std::cout << "   " << nodes.size() << " " << cells.size() << " " << bs
//               << std::endl;
//     std::cout << "   "
//               << "Matrix sizes" << std::endl;
//     std::cout << "   " << m << " " << n << std::endl;
//     std::cout << "   "
//               << "Entries set:" << std::endl;
//     std::cout << "   " << J.non_zeros() << std::endl;

//     auto col_block = ElementaryColBlock<bs>::Ones();
//     RHS = 0.;  // Set all values to zero
//     auto v = RHS.extract(cells);
//     v(0) = col_block;

//     Solver solver;
//     solver.compute(J);
//     OnePhysicsVector<bs> x = solver.solve(RHS);
//     std::cout << "||RHS-Ax|| = " << solver.residual(RHS) << std::endl;
//     // x is a OnePhysicsVector with the same structure as RHS
//     auto x_nodes = x.extract(nodes);
//     ElementaryColBlock<bs> xn0 = x_nodes(0);

//     auto itx = x.vector->begin(), its = solver.solution->begin();
//     while (itx != x.vector->end() && its != solver.solution->end()) {
//         assert(*itx == *its);
//         ++itx;
//         ++its;
//     }
// }

template <size_type bs>
static void test_one_physics_block_matrix() {
    auto&& [sites, cells, nodes, CN_con, NC_con] = make_mesh(21);

    auto diag_block = 2.0 * ElementaryBlock<bs, bs>::Identity();
    auto id = ElementaryBlock<bs, bs>::Identity();
    auto zeros = ElementaryBlock<bs, bs>::Zero();
    auto&& [J, RHS] = make_block_linear_system<bs>(sites);

    auto J_CC = J.extract(cells, cells);
    auto J_NN = J.extract(nodes, nodes);
    auto J_CN = J.extract(cells, nodes);
    auto J_NC = J.extract(nodes, cells);

    for (auto&& i : cells) J_CC(i, i) += diag_block;

    for (auto&& i : nodes) J_NN(i, i) += diag_block;

    for (auto&& [i, neighbours] : CN_con.targets_by_source()) {
        for (auto&& j : neighbours) J_CN(i, j) -= id;
    }

    for (auto&& [i, neighbours] : NC_con.targets_by_source()) {
        for (auto&& j : neighbours) J_NC(i, j) -= id;
    }

    // check the filling of J
    // cells, nodes are a partition of sites
    CHECK(cells.size() + nodes.size() == sites.size());
    CHECK(icus::intersect(cells, nodes, sites).empty());
    for (auto&& e : J.coo->get_entries()) {
        if (e.row() == e.col()) {
            CHECK(e.value() == diag_block);
        } else {
            CHECK(e.value() == -1 * id);
        }
    }
    // compute the non zero number of contributions
    auto nnz = sites.size();
    for (auto&& targets : CN_con.targets()) nnz += targets.size();
    for (auto&& targets : NC_con.targets()) nnz += targets.size();
    CHECK(J.non_zeros() == nnz);

    auto&& [m, n] = J.size();
    std::cout << "Block storage test:" << std::endl;
    std::cout << "   "
              << "Nodes, cells, block size" << std::endl;
    std::cout << "   " << nodes.size() << " " << cells.size() << " " << bs
              << std::endl;
    std::cout << "   "
              << "Matrix sizes" << std::endl;
    std::cout << "   " << m << " " << n << std::endl;
    std::cout << "   "
              << "Entries set:" << std::endl;
    std::cout << "   " << J.non_zeros() << std::endl;

    J.dump("dumptest.txt");

    auto col_block = ElementaryColBlock<bs>::Ones();
    RHS = 0.;
    auto v = RHS.extract(cells);
    v(0) = col_block;

    Solver solver;
    solver.compute(J);
    auto x = solver.solve(RHS);
    std::cout << "||RHS-Ax|| = " << solver.residual(RHS) << std::endl;
    // x is a OnePhysicsVector with the same structure as RHS
    auto x_nodes = x.extract(nodes);
    ElementaryColBlock<bs> xn0 = x_nodes(0);
    for (size_type i = 0; i < bs; ++i) {
        CHECK(std::abs(xn0(i) - 0.909091) < 1e-5);
    }

    auto itx = x.vector->begin(), its = solver.solution->begin();
    while (itx != x.vector->end() && its != solver.solution->end()) {
        CHECK(*itx == *its);
        ++itx;
        ++its;
    }

    std::vector<icus::index_type> drows_pT{{0, 1}};
    J.set_rows_to_identity(drows_pT);
    std::vector<icus::index_type> rows{{3, 4, 5}};
    J.template set_rows_to_identity<0>(rows);

    for (auto&& e : J.coo->get_entries()) {
        // these blocs are set to identity at the diag bloc, 0 elsewhere
        if (std::find(drows_pT.begin(), drows_pT.end(), e.row()) !=
            drows_pT.end()) {
            if (e.row() == e.col()) {
                CHECK(e.value() == id);
            } else {
                CHECK(e.value() == zeros);
            }
            // these blocs, the line 0 is set to identity at the diag bloc, 0
            // elsewhere, others block lines not modified
        } else if (std::find(rows.begin(), rows.end(), e.row()) != rows.end()) {
            if (e.row() == e.col()) {  // diag block
                for (size_type i = 0; i < bs; ++i) {
                    for (size_type j = 0; j < bs; ++j) {
                        if (i == 0 && j == 0) {
                            CHECK(e.value()(i, j) == 1);
                        } else if (i == j) {
                            CHECK(e.value()(i, j) == 2);
                        } else {
                            CHECK(e.value()(i, j) == 0);
                        }
                    }
                }
            } else {  // non diag block
                for (size_type i = 0; i < bs; ++i) {
                    for (size_type j = 0; j < bs; ++j) {
                        if (i == 0) {
                            CHECK(e.value()(i, j) == 0);
                        } else if (i == j) {
                            CHECK(e.value()(i, j) == -1);
                        } else {
                            CHECK(e.value()(i, j) == 0);
                        }
                    }
                }
            }
        } else {
            if (e.row() == e.col()) {
                CHECK(e.value() == diag_block);
            } else {
                CHECK(e.value() == -1 * id);
            }
        }
    }

    // std::cout << J << std::endl;
}

TEST_CASE("test one physics block matrix") {
    // test_one_physics_matrix<1>();
    // std::cout << "----------------" << std::endl;
    // test_one_physics_matrix<2>();
    // std::cout << "----------------" << std::endl;
    test_one_physics_block_matrix<1>();
    std::cout << "----------------" << std::endl;
    test_one_physics_block_matrix<3>();
}
