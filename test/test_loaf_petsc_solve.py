import sys
import pytest

pytestmark = pytest.mark.skipif(
    sys.platform == "win32", reason="does not run on windows"
)

from loaf import make_laplacian, make_block_linear_system
import loaf

if loaf.with_petsc:
    from loaf.petsc_solver import PetscSolver, PetscDirectSolver
from icus import ISet


def make_petsc_solve(matrix, RHS, solver):

    print("Number of sites:", matrix.size[0])
    print("Block size:", matrix.block_size)

    solver.set_up(matrix)
    x, status = solver.solve(RHS)
    print(status)

    print("||b-Ax|| = ", solver.residual())
    print("-------------------------------")

    try:
        import numpy as np
        import matplotlib.pyplot as plt

        sols = []
        for i in range(matrix.block_size):
            sols.append(list(x.as_array()[i :: matrix.block_size]))
        fig, ax = plt.subplots()
        for sol in sols:
            ax.plot(sol, marker="o")

        fig.savefig("plot.png")
    except ModuleNotFoundError:
        pass

    return x


# def test_scalar():
#     N = 10
#     block_size = 3
#     # lap is a OnePhysicsCOO matrix
#     lap, RHS = make_linear_system(ISet(N), block_size)
#     make_laplacian(lap)
#     for i in range(block_size):
#         RHS.as_array()[block_size * N - 1 - i] = (i + 1) * 1.0 / block_size
#     print("Solve from scalar storage")
#     make_petsc_solve(lap, RHS, PetscSolver())
#     make_petsc_solve(lap, RHS, PetscDirectSolver())


def test_block():
    N = 10
    block_size = 1
    # lap is a OnePhysicsBlockCOO matrix
    lap, RHS = make_block_linear_system(ISet(N), block_size)
    make_laplacian(lap)
    for i in range(block_size):
        RHS.as_array()[block_size * N - 1 - i] = (i + 1) * 1.0 / block_size
    print("Solve from block storage")
    make_petsc_solve(lap, RHS, PetscSolver())
    make_petsc_solve(lap, RHS, PetscDirectSolver())


if __name__ == "__main__":
    # test_scalar()
    test_block()
