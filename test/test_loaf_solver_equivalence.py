import sys
import pytest

pytestmark = pytest.mark.skipif(
    sys.platform == "win32", reason="does not run on windows"
)

from loaf import make_laplacian, make_block_linear_system
from loaf.eigen_solver import EigenSolver
import loaf

if loaf.with_petsc:
    from loaf.petsc_solver import PetscSolver, PetscDirectSolver
from icus import ISet
from numpy.linalg import norm


def make_solve(matrix, RHS, solver):
    solver.set_up(matrix)
    x, _ = solver.solve(RHS)
    return x


def test_solvers_give_same_result():
    N = 200
    block_size = 3
    size = N * block_size
    # lap is a OnePhysicsBlockCOO matrix
    lap, RHS = make_block_linear_system(ISet(N), block_size)
    make_laplacian(lap)

    for i in range(block_size):
        RHS.as_array()[block_size * N - 1 - i] = (i + 1) * 1.0 / block_size

    eigen_x = make_solve(lap, RHS, EigenSolver())

    petsc_x = make_solve(lap, RHS, PetscDirectSolver())

    assert norm(eigen_x.as_array() - petsc_x.as_array()) < 1.0e-10


def test_matrix_fill():
    N = 10
    block_size = 3
    size = N * block_size
    # lap is a OnePhysicsBlockCOO matrix
    lap, RHS = make_block_linear_system(ISet(N), block_size)
    make_laplacian(lap)
    for i in range(block_size):
        RHS.as_array()[block_size * N - 1 - i] = (i + 1) * 1.0 / block_size
    x0 = make_solve(lap, RHS, PetscDirectSolver()).as_array().copy()
    x1 = make_solve(lap, RHS, PetscDirectSolver()).as_array().copy()
    x2 = make_solve(lap, RHS, PetscSolver()).as_array().copy()
    assert norm(x0 - x1) < 1.0e-10
    assert norm(x0 - x2) < 1.0e-10


if __name__ == "__main__":
    test_matrix_fill()
    test_solvers_give_same_result()
