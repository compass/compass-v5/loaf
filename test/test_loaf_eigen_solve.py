import pytest

import copy

from icus import ISet

from loaf import make_laplacian, make_block_linear_system
from loaf.eigen_solver import EigenSolver


def make_eigen_solve(lap, RHS, block_size):
    solver = EigenSolver()
    solver.set_up(lap)
    x, status = solver.solve(RHS)
    print(status)
    assert status.solve_success
    assert solver.residual(RHS) < 1e-14


def do_some_plot(lap, RHS, block_size):
    solver = EigenSolver()
    solver.set_up(lap)
    x = RHS.as_array()
    try:
        import numpy as np
        import matplotlib.pyplot as plt

        sols = []
        for i in range(block_size):
            sols.append(list(x[i::block_size]))
        fig, ax = plt.subplots()
        for sol in sols:
            ax.plot(sol, marker="o")

        fig.savefig("plot.png")
    except ModuleNotFoundError:
        pass


def test_block_solve(plot=False):
    print("Eigen solver from block coo storage:")
    N = 10
    block_size = 3
    # lap is a OnePhysicsBlockCOO matrix
    lap, RHS = make_block_linear_system(ISet(N), block_size)
    for i in range(block_size):
        RHS.as_array()[block_size * N - 1 - i] = (i + 1) * 1.0 / block_size
    make_laplacian(lap)
    make_eigen_solve(lap, RHS, block_size)
    if plot:
        do_some_plot(lap, RHS, block_size)
    print("---------------------")


def check_solving(solver, mat, rhs):
    solver.set_up(mat)
    x, status = solver.solve(rhs)
    assert status.solve_success
    assert solver.residual(rhs) < 1e-14


@pytest.mark.parametrize(
    "N,block_size",
    [
        (10, 1),
        (10, 2),
        (10, 3),
    ],
)
def test_new(N, block_size):
    lap, RHS = make_block_linear_system(ISet(N), block_size)
    make_laplacian(lap)
    for i in range(block_size):
        RHS.as_array()[block_size * N - 1 - i] = (i + 1) * 1.0 / block_size
    #
    solver = EigenSolver()
    check_solving(solver, lap, RHS)
    check_solving(copy.copy(solver), lap, RHS)


if __name__ == "__main__":
    test_block_solve(plot=True)
