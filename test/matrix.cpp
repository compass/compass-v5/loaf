// #include "loaf/matrix.h"

#include <cassert>
#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <numeric>
#include <tuple>

#include "icus/Field.h"
#include "loaf/block_matrix.h"
#include "loaf/solve.h"
#include "mesh.h"

using namespace loaf;

// template <size_type block_rows, size_type block_cols>
// void test_scalar_accessors() {
//     using Matrix = Matrix<block_rows, block_cols>;
//     using EBlock = Matrix::block_type;

//     auto block = EBlock::Ones();
//     Matrix B(sites);
//     B(0, 0) += EBlock::Identity();  // rvalue
//     B(1, 0) -= block;
//     B(1, 0) += 1.618;       // All values of B(1, 0) are set to 1.618
//     B(1, 1, 0, 0) = 1.414;  // Access B(1, 1)(0, 0)
//     CHECK(B.non_zeros() == 3 * block_rows * block_cols + 1);

//     auto Bex = B.extract(cells, nodes);
//     Bex(0, 0) += EBlock::Identity();  // rvalue
//     Bex(1, 0) += block;
//     Bex(1, 0) -= 1.618;       // All values of Bex(1, 0) are set to 1.618
//     Bex(1, 1, 0, 0) = 1.414;  // Access Bex(1, 1)(0, 0)
//     CHECK(B.non_zeros() == 6 * block_rows * block_cols + 2);
//     B.dump("dumptest.txt");
//     B.print();

//     Bex.reset_col(0);
//     // CHECK(B.get_col(nodes.mapping(0)).size() == 0);

//     B.set_zero();
//     CHECK(B.non_zeros() == 0);
// }

// called with cells = sites.extract(0, N / 2)
// and nodes = sites.extract(N / 2, N)
template <size_type block_rows, size_type block_cols>
static void test_block_accessors(auto sites, auto cells, auto nodes) {
    using Matrix = BlockMatrix<block_rows, block_cols>;
    using EBlock = Matrix::block_type;

    auto block = EBlock::Ones();
    auto zeros = EBlock::Zero();
    Matrix B(sites);
    B(0, 0) += EBlock::Identity();  // rvalue
    B(1, 0) -= block;
    B(1, 1)(0, 0) = 1.414;  // Additive contribution to B(1, 1)(0, 0)
    CHECK(B.non_zeros() == 3);

    auto Bex = B.extract(cells, nodes);
    Bex(0, 0) += EBlock::Identity();  // rvalue
    Bex(1, 0) += block;
    Bex(0, 1) += 1 / 2 * EBlock::Identity();  // rvalue
    // Access Bex(2, 2)(0, 0), additive contribution
    Bex(2, 2)(0, 0) = 1. / 3.0;
    CHECK(B.non_zeros() == 7);
    B.dump("dumptest.txt");
    B.print();
    Bex(2, 2)(0, 0) += 1. / 4.0;  // additive contribution
    CHECK(B.non_zeros() == 8);

    std::vector<index_type> zero_row = {2};
    std::vector<index_type> zero_col = {1};
    auto zero_sites_row = cells.mapping()[2];
    // add a contribution outside of Bex, in the row where Bex will be nullified
    // (this value should exist after nullify)
    B(zero_sites_row, 0) += 2 * block;
    // otherwise the value checked later is not true anymore
    CHECK(zero_sites_row > 1);
    // nullify the entry (row, col) if row is in the list and col in the matrix
    // extraction
    Bex.set_rows_to_zero(zero_row);
    // nullify value Bex(0, 1)
    Bex.set_cols_to_zero(zero_col);

    CHECK(B.non_zeros() == 9);
    // sum Bex(2, 2) (values exist even if they are null)
    B.sort_and_add_entries();
    CHECK(B.non_zeros() == 8);

    // check that outside of Bex, the contribution still exists
    bool still_exists = false;
    // the extraction fills the same COO
    for (auto&& [be, be_ex] :
         std::views::zip(B.coo->get_entries(), Bex.coo->get_entries())) {
        // "==" check row and col equality
        CHECK(be == be_ex);
        CHECK(be.value() == be_ex.value());
        // check that value outside of Bex still exists
        if (be.row() == zero_sites_row && be.col() == 0) {
            still_exists = true;
            CHECK(be.value() == 2 * block);
        } else if (be.row() == zero_sites_row &&
                   be.col() == nodes.mapping()[2]) {
            // check that value of Bex(2, 2) is nullified,
            // not important if it does not exist anymore
            CHECK(be.value() == zeros);
        } else if (be.row() == cells.mapping()[0] &&
                   be.col() == nodes.mapping()[1]) {
            // check that value of Bex(0, 1) is nullified,
            // not important if it does not exist anymore
            CHECK(be.value() == zeros);
        }
    }
    CHECK(still_exists);

    // remove all the COO contributions because set_zero is applied on the whole
    // matrix (no extraction)
    B.set_zero();
    CHECK(B.non_zeros() == 0);
}

// called with cells = sites.extract(0, N / 2)
// and nodes = sites.extract(N / 2, N)
template <size_type block_rows, size_type block_cols>
static void test_set_identity(auto sites, auto cells) {
    using Matrix = BlockMatrix<block_rows, block_cols>;
    using EBlock = Matrix::block_type;

    CHECK(sites.size() > 6);
    auto block = EBlock::Ones();
    auto zeros = EBlock::Zero();
    Matrix B(sites);
    B(0, 0) += 3 * block;
    B(0, 1) -= EBlock::Identity();
    B(4, 6) -= block;
    B(0, 4) -= 4 * EBlock::Identity();
    B(0, 5) -= 5 * EBlock::Identity();
    B(6, 0) += 7 * block;
    B(4, 0) += 2 * EBlock::Identity();
    B(4, 3) += 3 * EBlock::Identity();
    B(1, 0) -= EBlock::Identity();
    B(1, 1) += 4 * block;
    B(4, 4) += 5 * block;
    B(3, 2) += 6 * block;
    B(1, 1) += 3 * block;
    B(5, 4) += 7 * block;
    B(4, 6) -= EBlock::Identity();
    B(1, 1) += EBlock::Identity();
    B(6, 6) += 8 * EBlock::Identity();
    B(6, 0) -= 3.14 * block;
    B(0, 6) -= 6 * EBlock::Identity();
    B(5, 4) -= 2 * block;
    std::vector<index_type> B_id_rows = {1, 3, 6};
    B.set_rows_to_diagonal(B_id_rows, 12.7);
    std::vector<index_type> B1_id_rows = {4};
    // modify only the line 1 of each block
    B.template set_rows_to_identity<1>(B1_id_rows);

    auto C = B.extract(cells, cells);
    // while checking the value after, we suppose that:
    CHECK(cells.mapping()[0] == 0);
    // test with an iset as argument
    C.set_rows_to_identity(cells.extract(std::vector<index_type>{0}));
    // modify only the line 2 of each block
    auto C2_iset_rows = cells.extract(std::vector<index_type>{2});
    // while checking the value after, we suppose that:
    CHECK(cells.mapping()[2] == 2);
    C.template set_rows_to_diagonal<2>(C2_iset_rows, 3.14);

    B.sort_and_add_entries();

    auto is_id_diag = icus::make_field<bool>(sites);
    is_id_diag.fill(false);

    for (auto&& be : B.coo->get_entries()) {
        // check values modified (or not) with C.set_rows_to_identity
        if (be.row() == 0) {
            if (be.row() == be.col()) {
                CHECK(be.value() == EBlock::Identity());
                is_id_diag(0) = true;
            } else if (be.col() < cells.size()) {
                CHECK(be.value() == zeros);
            } else if (be.col() == 6) {
                // check that the following value is outside of C, then not
                // modified
                CHECK(cells.size() <= 6);
                CHECK(be.value() == -6 * EBlock::Identity());
            }
        } else if (be.row() == 4) {
            // check values modified (or not) with B.template
            // set_rows_to_identity<1>
            if (be.row() == be.col()) {
                // B(4, 4) : line 1 is modified to the identity, others remain
                for (index_type i = 0; i < block_rows; ++i) {
                    for (index_type j = 0; j < block_cols; ++j) {
                        if (i == 1 && j == 1) {
                            CHECK(be.value()(i, j) == 1);
                        } else if (i == 1) {
                            CHECK(be.value()(i, j) == 0);
                        } else {
                            CHECK(be.value()(i, j) == 5);
                        }
                    }
                }
            } else if (be.col() == 6) {
                // B(4, 6) : line 1 is nullified, others remain, contributions
                // have been summed
                for (index_type i = 0; i < block_rows; ++i) {
                    for (index_type j = 0; j < block_cols; ++j) {
                        if (i == 1) {
                            CHECK(be.value()(i, j) == 0);
                        } else if (i == j) {
                            CHECK(be.value()(i, j) == -2);
                        } else {
                            CHECK(be.value()(i, j) == -1);
                        }
                    }
                }
            }
        } else if (be.row() == 2) {
            // check value is added with C.template set_rows_to_diagonal<2>
            // "added" because no value given in B(2,:)
            CHECK(be.col() == 2);
            // B(2, 2) : line 2 is modified to 3.14, others are null
            for (index_type i = 0; i < block_rows; ++i) {
                for (index_type j = 0; j < block_cols; ++j) {
                    if (i == 2 && j == 2) {
                        CHECK(be.value()(i, j) == 3.14);
                    } else {
                        CHECK(be.value()(i, j) == 0);
                    }
                }
            }
        } else if (std::find(B_id_rows.begin(), B_id_rows.end(), be.row()) !=
                   B_id_rows.end()) {
            // check values modified (or not) with B.set_rows_to_diagonal
            if (be.row() == be.col()) {
                CHECK(be.value() == 12.7 * EBlock::Identity());
                is_id_diag(be.row()) = true;
            } else {
                CHECK(be.value() == zeros);
            }
        } else if (be.row() == 5 && be.col() == 4) {
            // check that value is not modified (the 2 contributions are summed)
            CHECK(be.value() == 5 * block);
        }
    }
    CHECK((is_id_diag(0) && is_id_diag(1) && is_id_diag(3) && is_id_diag(6)));

    C.set_identity();
    // reset is_id_diag
    is_id_diag.fill(false);
    // check values modified (or not) with C.set_identity
    for (auto&& be : B.coo->get_entries()) {
        if (be.row() == 0) {
            if (be.row() == be.col()) {
                CHECK(be.value() == EBlock::Identity());
                is_id_diag(0) = true;
            } else if (be.col() < cells.size()) {
                CHECK(be.value() == zeros);
            } else if (be.col() == 6) {
                // check that the following value is outside of C, then not
                // modified
                CHECK(cells.size() <= 6);
                CHECK(be.value() == -6 * EBlock::Identity());
            }
        } else if (be.row() < cells.size()) {
            if (be.row() == be.col()) {
                CHECK(be.value() == EBlock::Identity());
                is_id_diag(be.row()) = true;
            } else if (be.col() < cells.size()) {
                CHECK(be.value() == zeros);
            }
            // check that the following value is outside of C, then not modified
        } else if (be.row() == 5 && be.col() == 4) {
            CHECK(cells.size() <= 5);
            // check that value is not modified (the 2 contributions are summed)
            CHECK(be.value() == 5 * block);
        } else if (be.row() == 0 && be.col() == 6) {
            CHECK(be.value() == -6 * EBlock::Identity());
        }
    }
}

template <size_type block_size>
static void test_vector_accessors(auto sites, auto nodes) {
    using Vector = OnePhysicsVector<block_size>;
    using EBlock = Vector::block_type;

    auto col_block = EBlock::Ones();
    Vector v(sites);
    v = 0.;
    v(0) = col_block;
    v(0) += 42. * EBlock::Ones();  // rvalue
    v(1) = 11. * EBlock::Ones();
    v(1) += col_block;  // lvalue
    EBlock b = v(0);    // make a copy
    CHECK(std::abs(b(0) - 43.) < 1.e-12);
    v(0)(0) = 3.14;
    CHECK(std::abs(b(0) - 43.) < 1.e-12);
    CHECK(std::abs(v(0)(0) - 3.14) < 1.e-12);

    auto v_nodes = v.extract(nodes);
    v_nodes(0) = col_block;
    v_nodes(0) += 42. * EBlock::Ones();  // rvalue
    v_nodes(1) = 11. / 3. * EBlock::Ones();
    v_nodes(1) += col_block;  // lvalue
    EBlock bb = v_nodes(0);
    for (index_type i = 0; i < block_size; ++i) {
        CHECK(bb(i) == 43);
    }

    v.dump("dumptest.txt");
    v.print();

    size_type nnz_count = 0;
    for (auto&& k : sites) {
        for (index_type i = 0; i < block_size; ++i)
            if ((*v.vector)[k * block_size + i] > 0) nnz_count += 1;
    }
    CHECK(nnz_count == 4 * block_size);
}

template <size_type block_size>
static void test_symetric(auto sites) {
    using Matrix = BlockMatrix<block_size, block_size>;
    using EBlock = Matrix::block_type;

    auto block = EBlock::Ones();
    auto zeros = EBlock::Zero();
    auto cross = EBlock::Identity() + EBlock::Identity().transpose();
    Matrix B(sites);
    B(0, 0) += EBlock::Identity();  // rvalue
    CHECK(B.is_symetric());         // careful, costly
    B(0, 1) += zeros;
    CHECK(B.is_symetric());  // careful, costly
    B(1, 0) -= block;
    CHECK_FALSE(B.is_symetric());  // careful, costly
    B(0, 1) -= block;
    B.sort_and_add_entries();
    CHECK(B.is_symetric());  // careful, costly
    B(1, 1)(0, 0) = 1.414;   // Additive contribution to B(1, 1)(0, 0)
    CHECK(B.is_symetric());  // careful, costly
    B(1, 3) -= 24.7 * block;
    B(2, 3) += 3.14 * cross;
    B(3, 1) -= 24.7 * block;
    CHECK_FALSE(B.is_symetric());  // careful, costly
    B(3, 2) += 3.14 * cross;
    CHECK(B.is_symetric());  // careful, costly
    B(3, 3) = 7.9 * EBlock::Identity();
    B(5, 5) = 7.3 * cross;
    CHECK(B.is_symetric());  // careful, costly
}

TEST_CASE("test block matrix vector accessors") {
    auto&& [sites, cells, nodes, CN_con, NC_con] = make_mesh(7);

    // test_scalar_accessors<1, 3>();
    // std::cout << "-------------------------" << std::endl;
    // test_scalar_accessors<2, 1>();
    // std::cout << "-------------------------" << std::endl;
    // test_scalar_accessors<3, 2>();
    // std::cout << "-------------------------" << std::endl;
    test_block_accessors<1, 3>(sites, cells, nodes);
    std::cout << "-------------------------" << std::endl;
    test_block_accessors<2, 1>(sites, cells, nodes);
    std::cout << "-------------------------" << std::endl;
    test_block_accessors<2, 2>(sites, cells, nodes);
    std::cout << "-------------------------" << std::endl;
    test_block_accessors<3, 2>(sites, cells, nodes);
    std::cout << "-------------------------" << std::endl;
    test_vector_accessors<1>(sites, nodes);
    std::cout << "-------------------------" << std::endl;
    test_vector_accessors<2>(sites, nodes);
    std::cout << "-------------------------" << std::endl;
    test_vector_accessors<3>(sites, nodes);
    std::cout << "-------------------------" << std::endl;
    test_set_identity<3, 3>(sites, cells);
    std::cout << "-------------------------" << std::endl;
    test_set_identity<4, 3>(sites, cells);
    std::cout << "-------------------------" << std::endl;
    test_symetric<3>(sites);
}
