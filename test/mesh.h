#pragma once

#include <icus/Connectivity.h>
#include <icus/ISet.h>
#include <icus/types.h>

namespace loaf {

static auto make_mesh(icus::size_type N) {
    if (N % 2 == 0) {
        N = N + 1;
    }
    icus::ISet sites(N);
    auto cells = sites.extract(0, N / 2);
    auto nodes = sites.extract(N / 2, N);
    assert(sites == cells.parent());
    auto CN_connections = [](icus::size_type n) {
        std::vector<std::vector<icus::index_type>> cnc(n);
        for (icus::index_type i = 0; i < n; ++i) {
            cnc[i] = {i, i + 1};
        }
        return cnc;
    };
    auto CN_con =
        icus::Connectivity(cells, nodes, CN_connections(cells.size()));
    auto NC_con = CN_con.transpose();

    return std::make_tuple(sites, cells, nodes, CN_con, NC_con);
}
}  // namespace loaf
