import sys
import pytest

pytestmark = pytest.mark.skipif(
    sys.platform == "win32", reason="does not run on windows"
)

from loaf import make_laplacian, make_block_linear_system
import loaf

if loaf.with_petsc:
    from loaf.petsc_solver import PetscLinearSystem
from icus import ISet


def test_block():
    N = 10
    block_size = 3
    # lap is a OnePhysicsCOO matrix
    lap, RHS = make_block_linear_system(ISet(N), block_size)
    make_laplacian(lap)
    for i in range(block_size):
        RHS.as_array()[block_size * N - 1 - i] = (i + 1) * 1.0 / block_size
    ls = PetscLinearSystem()
    ls.fill(lap)


if __name__ == "__main__":
    test_block()
