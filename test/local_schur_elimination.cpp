#include <catch2/catch_test_macros.hpp>
#include <iostream>

#include "loaf/elementary_block.h"
#include "loaf/linear_block_elimination.h"

struct State {
    double x, y, z;
};

/** Closure elimination for "y + z = 1", keeping "(x, y)" as main unknowns
 *  Using Schur complement
 *
 * C = [0 1 1] ; B = [1]
 * C * U = B -> y + z = 1
 * C = [C1|C2] -> C1 = [0 1] ; C2 = [1]
 *
 *               [       Id_2       ]
 *  mat_factor = [ ---------------- ]    shape : (3, 2)
 *               [ -(C2 ** -1) * C1 ]
 *
 *               [       0         ]
 *  rhs_factor = [ --------------- ]     shape : (3, 1)
 *               [ -(C2 ** -1) * B ]
 *
 */
struct ClosureElimination {
    using eliminator_t = loaf::LinearBlockEliminator<2, 3>;
    template <typename Context, typename Rocktype, typename State>
    void update_local_elimination(eliminator_t& eliminator, const Context&,
                                  const Rocktype&, const State&) {
        // N.B : nameless variable to avoid warnings in Debug
        eliminator.mat_factor << 1, 0, 0, 1, 0, -1;
        eliminator.rhs_factor << 0, 0, -1;
    }
};

template <typename A_t>
bool is_zero(const A_t& A, double eps = 1e-6) {
    return A.cwiseAbs().maxCoeff() < eps;
}

template <typename A_t, typename B_t>
bool is_close(const A_t& A, const B_t& B, double eps = 1e-6) {
    return is_zero(A - B, eps);
}

template <typename A_t, typename B_t>
auto solve(const A_t& A, const B_t& B) {
    return A.partialPivLu().solve(B);
    /* return A.fullPivLu().solve(B) ; */
}

TEST_CASE("test local schur elimination") {
    auto closure = ClosureElimination();
    ClosureElimination::eliminator_t elim;

    closure.update_local_elimination(elim, 0, 'a', State{1, 2, 3});
    loaf::ElementaryBlock<2, 3> J;
    J << 1, 0, 1, 0, 1, 0;
    loaf::ElementaryBlock<2, 1> b;
    b << 4, 6;

    // for validation
    loaf::ElementaryBlock<2, 2> J_incr;
    J_incr << 1, -1, 0, 1;
    loaf::ElementaryBlock<2, 1> b_incr;
    b_incr << -1, 0;

    CHECK(is_close(elim.mat_incr(J), J_incr));
    CHECK(is_close(elim.rhs_incr(J), b_incr));

    loaf::ElementaryBlock<3, 3> J_full;
    J_full({0, 1}, {0, 1, 2}) = J;
    J_full(2, {0, 1, 2}) << 0, 1, 1;
    loaf::ElementaryBlock<3, 1> b_full;
    b_full({0, 1}) = b;
    b_full(2) = 1;

    std::cout << J_full << "\n";
    std::cout << b_full << "\n";

    /* auto u_full = solve(J_full, b_full) ;  // fails : misusing eigen ? */
    loaf::ElementaryBlock<3, 1> u_full;
    u_full << 9, 6, -5;  // external computation
    std::cout << u_full << "\n";
    CHECK(is_zero(J_full * u_full - b_full));

    /* auto u_reduced = solve(J_incr, b + b_incr); */
    loaf::ElementaryBlock<2, 1> u_reduced;
    u_reduced << 9, 6;
    std::cout << u_reduced << "\n";
    CHECK(is_zero(J_incr * u_reduced - b - b_incr));
    auto u_reconstructed = elim.reconstruct(u_reduced);
    CHECK(is_zero(J_full * u_reconstructed - b_full));
    CHECK(is_close(u_reconstructed, u_full));
}
