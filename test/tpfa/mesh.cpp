#include <vector>

#include "icus/Connectivity.h"
#include "icus/ISet.h"
#include "icus/Property.h"

using namespace icus;

namespace mesh {

using index_type = ISet::index_type;
using size_type = ISet::size_type;

// Problem size
size_type N = 20;

// ISets indexing mesh items
auto cells = ISet(N);
auto faces = ISet(N + 1);

auto left_wall = faces.extract((ISet::index_type[]){0});
auto right_wall = faces.extract((ISet::index_type[]){N});
auto dirichlet_walls = join(left_wall, right_wall);
auto interior_faces = faces.extract(1, N);

using con_type = std::vector<std::vector<index_type>>;
// Mesh connectivities, face numbered k has neighbours cell k and k+1

void consecutive_neighbours(const size_type N, con_type &neighbourhood) {
    for (index_type k = 0; k < N; ++k) {
        neighbourhood.push_back({k, k + 1});
    }
}

con_type neighbours(const size_type N) {
    con_type an;
    an.reserve(2 * N);
    consecutive_neighbours(N, an);
    an.shrink_to_fit();
    return an;
}

con_type all_neighbours(size_type N) {
    con_type an{{0}};
    an.reserve(2 * N);
    consecutive_neighbours(N, an);
    an.push_back({N});
    an.shrink_to_fit();
    return an;
}

auto faces_by_cells = Connectivity(cells, faces, neighbours(N));
auto cells_by_faces = Connectivity(faces, cells, all_neighbours(N - 1));
auto cells_by_interior_faces = cells_by_faces.extract(interior_faces, cells);

// Wall faces have neighbours cell 0 (left wall) and cell N-1 (right wall)
auto cells_by_walls = cells_by_faces.extract(dirichlet_walls, cells);

// Space properties of the problem
// Center and width are defined for each cell
double domain_size = 10.0;
auto face_x = Property(faces) = 0;
auto wall_x = Property(dirichlet_walls) = (double[]){0, domain_size};
auto cell_width = Property(cells) = 0;
auto cell_center = Property(cells) = 0;
auto center_to_face_distance = make_property(cells_by_faces);
auto center_to_wall_distance = make_property(cells_by_walls);

// Physical properties
// Conductivity lambda is defined for each cell
auto lambda = make_property(cells);

double T_left = 10.0;
double T_right = 19.0;
auto wall_T = Property(dirichlet_walls) = (double[]){T_left, T_right};
}  // namespace mesh
