#include "loaf/tpfa.h"

#include <iostream>

#include "icus/Property.h"
#include "loaf/eigen_wrapper.h"
#include "mesh.h"
#include "utils.cpp"

int main() {
    using namespace icus;
    using namespace mesh;

    for (auto&& cell : cell_width.support) {
        cell_width(cell) = trapezoid(0.5, cell, N) * domain_size / N;
    }
    face_x = faces_x_from_width(faces_by_cells, cell_width);

    auto interior_face_x = face_x.extract(interior_faces);

    auto cell_center = shifted_cell_center_from_face_x(faces_by_cells, face_x);
    auto center_to_face_distance =
        cell_to_face_distance(cells_by_faces, cell_center, face_x);

    auto center_to_wall_distance =
        cell_to_face_distance(cells_by_walls, cell_center, wall_x);

    for (auto&& i : cells) {
        lambda(i) = step(1.0, 5.0, cell_center(i), domain_size);
    }

    // Linear algebra structures
    auto sites = make_family(std::array<ISet, 1>{{cells}});
    Matrix full_J{sites};
    Vector full_RHS{sites};

    // compute transmissivities
    auto faces_surface = Property(faces) = 1.0;
    auto Tf = tpfa::transmissivities(cells_by_faces, lambda, faces_surface,
                                     center_to_face_distance);

    // Matrix fill
    auto J = full_J.extract(cells, cells);
    auto Tf_interior = Tf.extract(interior_faces);
    tpfa::fill_interior_fluxes_contribution(J, cells_by_interior_faces,
                                            Tf_interior);

    // RHS fill
    auto RHS = full_RHS.extract(cells);
    auto Tf_walls = Tf.extract(dirichlet_walls);
    tpfa::fill_Dirichlet_fluxes_contribution(J, RHS, cells_by_walls, Tf_walls,
                                             wall_T);

    // Solve linear system and write solution to file
    auto sol = full_J.solve(full_RHS);

    write_to_file("sol.dat", domain_size, T_left, T_right, cell_center, face_x,
                  sol, lambda);

    return 0;
}
