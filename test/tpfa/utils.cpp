#include <Eigen/Dense>
#include <fstream>

#include "icus/Connectivity.h"
#include "icus/ISet.h"
#include "icus/Property.h"

using namespace icus;

Property<ISet, double> center_from_width(Property<ISet, double> &cell_width) {
    auto cell_center = make_property(cell_width.support);

    auto N = cell_center.size();
    auto cells = cell_center.support;
    cell_center(*(cells.begin())) = cell_width(*(cells.begin())) / 2.0;
    for (auto &&i : cells) {
        if (i > 0) {
            cell_center(i) =
                cell_center(i - 1) + 0.5 * (cell_width(i - 1) + cell_width(i));
        }
    }
    return cell_center;
};

Property<ISet, double> faces_x_from_width(
    Connectivity<ISet, ISet> &faces_by_cells,
    Property<ISet, double> &cell_width) {
    assert(faces_by_cells.source() == cell_width.support);
    Property<ISet, double> face_x(faces_by_cells.target());

    for (auto &&[i, neighbours] : faces_by_cells) {
        std::vector<Connection<mesh::index_type>> my_faces_j{};
        for (auto &&j : neighbours) {
            my_faces_j.push_back(j);
        }
        assert(my_faces_j.size() == 2);
        face_x(my_faces_j[1]) = face_x(my_faces_j[0]) + cell_width(i);
    }
    return face_x;
};

Property<ISet, double> shifted_cell_center_from_face_x(
    Connectivity<ISet, ISet> &faces_by_cells, Property<ISet, double> &face_x) {
    assert(faces_by_cells.target() == face_x.support);
    auto cell_center = Property(faces_by_cells.source()) = 0;
    std::vector<double> weights{0.3, 0.7};

    for (auto &&[i, neighbours] : faces_by_cells) {
        assert(neighbours.size() == 2);
        int tmp = 0;
        for (auto &&j : neighbours) {
            cell_center(i) += weights[tmp++] * face_x(j);
        }
    }
    return cell_center;
};

Property<Connectivity<ISet, ISet>, double> cell_to_face_distance(
    Connectivity<ISet, ISet> &cells_by_faces,
    Property<ISet, double> &cell_center, Property<ISet, double> &face_center) {
    assert(cells_by_faces.source() == face_center.support);
    assert(cells_by_faces.target() == cell_center.support);
    // assert(cell_to_face_distance.support == cells_by_faces); // qd il y aura
    // les pointeurs

    auto cell_to_face_distance = Property(cells_by_faces) = 0;
    for (auto &&[i, neighbours] : cells_by_faces) {
        for (auto &&j : neighbours) {
            cell_to_face_distance(i, j) = abs(face_center(i) - cell_center(j));
        }
    }
    return cell_to_face_distance;
};

double trapezoid(double lside, ISet::index_type i, ISet::index_type N) {
    // A function defined between i=0 and i=N
    // with integral 1 (trapezoid between lside and 2-lside)
    // Used for generating cells of linearly growing sizes
    assert(lside > 0.0 && lside <= 1.0);
    double x = (1.0 * i) / N + 1.0 / (2.0 * N);
    assert(x >= 0.0 && x <= 1.0);
    return (lside + x * 2 * (1 - lside));
};

double step(double left, double right, double x, double L) {
    // Step function used for generating a non-constant conductivity
    // Equals 'left' for x < L/2 and 'right' for L/2 < x < L
    assert(left > 0.0 && right > 0.0);
    assert(x <= L);
    if (x / L <= 0.5) {
        return left;
    } else {
        return right;
    }
};

// void write_to_file(std::string fsol, double domain_size, double T_left,
//                    double T_right, Property<ISet, double> &cell_center,
//                    Property<ISet, double> &cell_width, Eigen::VectorXd &sol,
//                    Property<ISet, double> lambda) {
//     std::ofstream solfile;
//     solfile.open(fsol);
//     auto N = cell_center.size();
//     solfile << "# Problem parameters:\n";
//     solfile << "# x_min, x_max, T_left, T_right\n";
//     solfile << 0.0 << "," << domain_size << "," << T_left << "," << T_right
//             << "\n\n";
//     solfile << "# Solution values\n";
//     solfile << "# x, dx(x), T(x), lambda(x)\n";
//     for (ISet::index_type i = 0; i < N; ++i) {
//         solfile << cell_center(i) << ", " << cell_width(i) << ", " << sol(i)
//                 << ", " << lambda(i) << "\n";
//         ;
//     }
//     solfile.close();
// }

void write_to_file(std::string fsol, double domain_size, double T_left,
                   double T_right, Property<ISet, double> &cell_center,
                   Property<ISet, double> &face_x, Eigen::VectorXd &sol,
                   Property<ISet, double> &lambda) {
    std::ofstream solfile;
    solfile.open(fsol);
    auto N = cell_center.size();
    solfile << "# Problem parameters:\n";
    solfile << "# x_min, x_max, T_left, T_right\n";
    solfile << 0.0 << "," << domain_size << "," << T_left << "," << T_right
            << "\n\n";
    solfile << "# Solution values\n";
    solfile << "# x(i), l_face(i), r_face(i), T(x), lambda(x)\n";
    for (ISet::index_type i = 0; i < N; ++i) {
        solfile << cell_center(i) << ", " << face_x(i) << ", " << face_x(i + 1)
                << ", " << sol(i) << ", " << lambda(i) << "\n";
    }
    solfile.close();
}
