#pragma once

#include "eigen_wrapper.h"
#include "icus/Connectivity.h"
#include "icus/ISet.h"
#include "icus/Property.h"
#include "icus/detail/CRS.h"

using namespace icus;

namespace tpfa {
// lamda is permeability for Darcy
// lamda is thermal conductivity for Fourier
// TODO: change if lambda is a tensor
template <typename value_type>
Property<ISet, value_type> transmissivities(
    Connectivity<ISet, ISet> &faces_x_cells,
    Property<ISet, value_type> &cells_lambda,
    Property<ISet, value_type> &faces_surface,
    Property<Connectivity<ISet, ISet>, value_type> &faces_x_cells_distance) {
    assert(have_same_support(faces_x_cells.source(), faces_surface));
    assert(have_same_support(faces_x_cells.target(), cells_lambda));
    assert(have_same_support(faces_x_cells, faces_x_cells_distance));

    auto Tf = make_property(faces_x_cells.source());
    double inv_heterogeneous_lambda;
    for (auto &&[f, neigh_cells] : faces_x_cells) {
        inv_heterogeneous_lambda = 0.0;
        for (auto &&K : neigh_cells) {
            inv_heterogeneous_lambda +=
                faces_x_cells_distance(f, K) / cells_lambda(K);
        }
        Tf(f) = faces_surface(f) / inv_heterogeneous_lambda;
    }
    return Tf;
};

template <typename Jacobian, typename value_type>
void fill_interior_fluxes_contribution(
    Jacobian &J, Connectivity<ISet, ISet> &faces_x_cells,
    Property<ISet, value_type> &transmissivities) {
    assert(have_same_support(faces_x_cells.source(), transmissivities));

    for (auto &&[f, neighbours] : faces_x_cells) {
        const auto C_KL = -2 * transmissivities(f);
        const auto [K, L] = get_all<2>(neighbours);
        J(K, K) += C_KL;
        J(L, K) -= C_KL;
        J(L, L) += C_KL;
        J(K, L) -= C_KL;
    }
}

template <typename Jacobian, typename RHS, typename value_type>
void fill_Dirichlet_fluxes_contribution(
    Jacobian &J, RHS &rhs, Connectivity<ISet, ISet> &faces_x_cells,
    Property<ISet, value_type> &transmissivities,
    Property<ISet, value_type> &Dirichlet_values) {
    assert(have_same_support(faces_x_cells.source(), transmissivities));
    assert(have_same_support(faces_x_cells.source(), Dirichlet_values));

    for (auto &&[f, neighbours] : faces_x_cells) {
        // J(f, f) = 1.0;
        // rhs(f) = Dirichlet_values(f);
        const auto Tf = transmissivities(f);
        const auto K = neighbours.template get_all<1>();
        J(K, K) -= 2 * Tf;
        rhs(K) += -2 * Tf * Dirichlet_values(f);
    }
}
}  // namespace tpfa
