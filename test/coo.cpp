#include <catch2/catch_test_macros.hpp>

#include "loaf/block_coo.h"
#include "loaf/elementary_block.h"
// #include "loaf/scalar_coo.h"

using namespace loaf;

// m, n size of the elementary block
template <size_t m, size_t n>
// N size of the matrix
static void test_block_coo_structure(size_type N) {
    using COO = ElementaryBlockCOO<m, n>;
    using EBlock = COO::entry_type;

    EBlock block;
    for (index_type i = 0; i < m; ++i) {
        for (index_type j = 0; j < n; ++j) {
            block(i, j) = (i + 1) * (j + 1);
        }
    }
    ElementaryBlockCOO<m, n> matrix(N, N);
    CHECK(matrix.is_constructed());

    matrix.add_entry(int(N / 2), int(N / 3), block);
    matrix.add_entry(0, 0, block);

    std::vector<typename COO::triplet> v(matrix.begin(), matrix.end());
    CHECK(v.size() == 2);
    auto& entries = matrix.get_entries();
    for (auto&& [a, b] : std::views::zip(v, entries)) {
        // "==" tests that rows and cols are equal, not values
        CHECK(a == b);
        CHECK(a.value() == b.value());
    }

    auto& b = matrix.get_new(N - 1, N - 1);
    b.setIdentity();
    auto l = matrix.get_last();
    CHECK(b == l);
    CHECK(l == EBlock::Identity());
    entries.back().split();

    CHECK(!matrix.is_set_up());
    matrix.sort_and_add_entries();
    CHECK(matrix.is_set_up());
    CHECK(entries.size() == 3);

    // add new contribution to (0, 0)
    matrix.add_entry(0, 0, 2 * block);
    CHECK(!matrix.is_set_up());
    CHECK(entries.size() == 4);

    matrix.sort_and_add_entries();

    // check matrix values
    for (auto&& e : entries) {
        if (e.row() == 0 && e.col() == 0) {
            CHECK(e.value() == 3 * block);
        } else if (e.row() == (index_type)(N / 2) &&
                   e.col() == (index_type)(N / 3)) {
            CHECK(e.value() == block);
        } else if (e.row() == N - 1 && e.col() == N - 1) {
            CHECK(e.value() == EBlock::Identity());
        } else {
            // should not have other contributions
            CHECK(false);
        }
    }

    // check rc_entry which look for element with row and col indices
    auto p = matrix.rc_entry(N - 1, N - 1);
    CHECK(((*p).row() == N - 1 && (*p).col() == N - 1));
    p = matrix.rc_entry(0, 0);
    CHECK(((*p).row() == 0 && (*p).col() == 0));
    // will not find the element, start after it
    auto q = matrix.rc_entry(0, 0, 1);
    // cannot check the following, entries is private
    // CHECK(q == matrix.entries.end());
    CHECK(q == matrix.get_entries().end());

    matrix.set_zero_if([](auto& t) { return t.row() < 1; });
    matrix.erase_if_contiguous([N](auto& t) { return t.row() == N - 1; });

    // check new matrix values
    for (auto&& e : entries) {
        if (e.row() == 0 && e.col() == 0) {
            CHECK(e.value() == EBlock::Zero());
        } else if (e.row() == (index_type)(N / 2) &&
                   e.col() == (index_type)(N / 3)) {
            CHECK(e.value() == block);
        } else if (e.row() == N - 1 && e.col() == N - 1) {
            CHECK(e.value() == EBlock::Zero());
        } else {
            // should not have other contributions
            CHECK(false);
        }
    }
}

// void test_coo_structure(size_type N) {
//     using COO = ScalarCOO;

//     value_type val(1.);
//     COO matrix(N, N);
//     CHECK(matrix.is_constructed());

//     matrix.add_entry(N / 2, N / 2, val);
//     matrix.add_entry(0, 0, 1.0);

//     std::vector<typename COO::triplet> v(matrix.begin(), matrix.end());
//     CHECK(v.size() == 2);
//     auto entries = matrix.get_entries();
//     for (auto&& [a, b] : std::views::zip(v, entries)) {
//         CHECK(a == b);
//     }

//     auto b = matrix.get_new(N - 1, N - 1);
//     auto l = matrix.get_last();
//     CHECK(b == l);
//     matrix.get_entries().back().split();

//     CHECK(!matrix.is_set_up());
//     matrix.sort_and_add_entries();
//     CHECK(matrix.is_set_up());

//     matrix.set_zero_if([](auto& t) { return t.row() < 1; });
//     matrix.erase_if_contiguous([N](auto& t) { return t.row() == N - 1; });
//     std::cout << matrix << std::endl;

//     // auto row0 = matrix.get_row(0);
//     // auto col0 = matrix.get_col(0);
// }

TEST_CASE("test block COO") {
    // test_coo_structure(10);
    test_block_coo_structure<1, 1>(10);
    test_block_coo_structure<2, 2>(20);
    test_block_coo_structure<2, 3>(12);
    test_block_coo_structure<3, 1>(24);
}
