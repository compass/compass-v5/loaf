from loaf import (
    ElementaryBlock22,
    Triplet,
    # OnePhysicsMatrix2,
    OnePhysicsBlockMatrix2,
    OnePhysicsVector2,
    make_laplacian,
    make_block_linear_system,
    make_OnePhysicsVector,
)
from icus import ISet


def test_matrix():
    block = ElementaryBlock22()
    print("Elementary block of size :", block.size())
    print("Uninitialized values :\n", block)
    print("---------------------")

    T = Triplet()
    print("Scalar triplet :")
    print(T)
    print(T.as_tuple())
    a, b, c = T.row, T.column, T.value
    print("---------------------")

    # A = OnePhysicsMatrix2(ISet(10))
    # A, RHS = make_linear_system(ISet(10), 2)
    # make_laplacian(A)
    # print("One physics scalar matrix :")
    # print("Size:", A.size)
    # print("Block size:", A.block_size)
    # print("Number of non zero entries:", A.non_zeros)
    # A.dump("dumptest.txt")
    # A.coo.sort_and_add_entries()
    # print("---------------------")

    v = OnePhysicsVector2(ISet(10))
    vb = make_OnePhysicsVector(ISet(5), 2)
    assert v.block_size == vb.block_size
    print("One physics scalar vector :")
    print("Size:", v.size)
    print("Block size:", v.block_size)
    # Writable reference to the internal values of the underlying VectorXd as a np.array
    npv = v.as_array()
    npv[0::2] = 27
    npv[1::2] = 61
    print(v.as_array())
    print(v.as_matrix())
    s = v.sites
    v.dump("dumptest.txt")


def test_block_matrix():
    # A = OnePhysicsBlockMatrix2()
    I = ISet(7)
    A = OnePhysicsBlockMatrix2(I)
    A, RHS = make_block_linear_system(I, 2)
    make_laplacian(A)
    print("One physics block matrix :")
    print("Size:", A.size)
    print("Block size:", A.block_size)
    print("Number of non zero entries:", A.non_zeros)
    J = I.slice(1, 3)
    K = I.slice(0, 2)
    B = A.extract(J, K)
    A.coo.sort_and_add_entries()
    B.set_zero()
    B.set_rows_to_diagonal([0, 1], 3.26)
    A.set_rows_to_identity([4])
    J = I.extract([4])
    A.set_rows_to_identity(J)
    A.set_rows_to_zero([5])
    A.set_cols_to_zero([5, 6])
    A.dump("dumptest.txt")
    A.set_identity()
    print("---------------------")


def test_factories():
    for i in range(1, 9):
        A, RHS = make_block_linear_system(ISet(10), i)
        assert A.block_size == i
        assert RHS.block_size == i

    # for i in range(1, 9):
    #     A, RHS = make_linear_system(ISet(10), i)
    #     assert A.block_size == i
    #     assert RHS.block_size == i
