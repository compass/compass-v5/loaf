LOAF (Linear Operator Access and Fill)
======================================

LOAF is the ComPASS subpackage in charge of linear algebra. It features data structures for storing the matrix and vector coefficients and utility methods to reach subblocks of the matrix using ICUS' ISets. Linear solving is performed through wrappers to external libraries like Eigen and PETSc.

LOAF objects are bound to Python using nanobind

Installation
============

```
# Configure
cmake -B build -Dnanobind_DIR=/where/to/find/nanobind -DCMAKE_BUILD_TYPE=Debug -DBUILD_PYTHON_BINDINGS=ON
# Compile
cmake --build build -j 4
```
To build python bindings, once the project has been built you can
either run python build commands in the python directory,
or use one of the cmake targets:

```
# Install development version of the module
cmake --build build --target develop_python
# Build python wheel
cmake --build build --target python_wheel
```

Python bindings
===============
Python bindings are generated using nanobind.
The compilation is driven by CMake which implies a few hacks in python/setup.py.
Currently the bindings are generated using static linking to nanobind.
Dynamic linking could be use if there are more than a single binding.
