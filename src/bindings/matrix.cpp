#include <icus/ISet.h>
#include <loaf/block_coo.h>
#include <loaf/block_matrix.h>
#include <loaf/factories.h>
// #include <loaf/matrix.h>
// #include <loaf/scalar_coo.h>
#include <loaf/vector.h>
#include <nanobind/eigen/dense.h>
#include <nanobind/nanobind.h>
#include <nanobind/ndarray.h>
#include <nanobind/stl/pair.h>
#include <nanobind/stl/shared_ptr.h>
#include <nanobind/stl/string.h>
#include <nanobind/stl/tuple.h>
#include <nanobind/stl/vector.h>

namespace nb = nanobind;
using namespace loaf;

template <typename V>
inline auto steal_buffer(V&& v) {
    using T = typename std::decay_t<V>::value_type;
    std::vector<T>* thief = new std::vector<T>{std::move(v)};
    assert(thief);
    nb::capsule owner(reinterpret_cast<void*>(thief), [](void* p) noexcept {
        delete reinterpret_cast<std::vector<T>*>(p);
    });
    return nb::ndarray<nb::numpy, T, nb::c_contig>(thief->data(),
                                                   {thief->size()}, owner);
}

template <typename T, typename... Ts>
void add_string_representation(nb::class_<T, Ts...>& cls) {
    cls.def("__str__", [](const T& self) {
        std::stringstream buffer;
        buffer << self;
        return buffer.str();
    });
}

template <typename COO>
auto bind_COO(nb::module_& module, std::string class_name) {
    using index_type = loaf::index_type;
    using size_type = loaf::size_type;

    auto cls = nb::class_<COO>(module, class_name.c_str());
    cls.def(nb::init<>());
    cls.def(nb::init<icus::ISet&, icus::ISet&>());
    cls.def(nb::init<size_type, size_type>());
    cls.def(nb::init<const COO&>());
    cls.def("size", &COO::size);
    cls.def("sort_and_add_entries", &COO::sort_and_add_entries);
    cls.def("nnz_per_row",
            [](const COO& self) { return steal_buffer(self.nnz_per_row()); });
    cls.def("check", &COO::check);
    add_string_representation(cls);

    return cls;
}
template <size_type bsize>
void bind_all_square_block_COO(nb::module_& module) {
    if constexpr (bsize > 0) {
        using BlockCOO = ElementaryBlockCOO<bsize, bsize>;
        std::string class_name =
            "BlockCOO_" + std::to_string(bsize) + "x" + std::to_string(bsize);
        auto cls = bind_COO<BlockCOO>(module, class_name);

        bind_all_square_block_COO<bsize - 1>(module);
    }
}

template <typename MatrixType>
void make_laplacian(MatrixType& laplacian) {
    constexpr auto bs = MatrixType::block_rows;
    auto&& [N, C] = laplacian.size();
    assert(N == C && "make_laplacian: matrix must be square");
    laplacian.set_zero();
    auto diag = 2. * ElementaryBlock<bs, bs>::Identity();
    auto non_diag = -1. * ElementaryBlock<bs, bs>::Identity();
    for (auto&& i : laplacian.row_sites) {
        laplacian(i, i) += diag;
        if (i > 0) laplacian(i, i - 1) += non_diag;
        if (i < N - 1) laplacian(i, i + 1) += non_diag;
    }
}

template <typename Matrix>
void bind_matrix(nb::module_& module, std::string class_name) {
    constexpr auto br = Matrix::block_rows;
    constexpr auto bc = Matrix::block_cols;
    auto cls = nb::class_<Matrix>(module, class_name.c_str());
    // cls.def(nb::init<>());
    cls.def(nb::init<icus::ISet&>());
    cls.def_ro("coo", &Matrix::coo);
    cls.def_ro("row_sites", &Matrix::row_sites);
    cls.def_ro("col_sites", &Matrix::col_sites);
    cls.def_prop_ro("size", &Matrix::size);
    cls.def_prop_ro("block_size",
                    [](Matrix& self) { return Matrix::block_rows; });
    cls.def_prop_ro("full_size", [](Matrix& self) {
        const auto [nr, nc] = self.size();
        return nb::make_tuple(nr * Matrix::block_rows, nc * Matrix::block_cols);
    });
    cls.def_prop_ro("non_zeros", &Matrix::non_zeros);
    cls.def("dump", &Matrix::dump);
    cls.def("add_entry",
            [](Matrix& self, const index_type row, const index_type col,
               nb::ndarray<value_type, nb::shape<br, bc>, nb::c_contig> block) {
                self(row, col) +=
                    Eigen::Map<typename Matrix::block_type>(block.data());
            });
    cls.def("add_entries",
            [](Matrix& self, nb::ndarray<index_type, nb::c_contig> indices,
               nb::ndarray<value_type, nb::c_contig> blocks) {
                const auto n = indices.shape(0);
                if (n != blocks.shape(0))
                    throw std::runtime_error("inconsitent shapes");
                const index_type* ij = indices.data();
                value_type* block = blocks.data();
                for (std::size_t k = 0; k != n; ++k) {
                    self(*ij, *(++ij)) +=
                        Eigen::Map<typename Matrix::block_type>(block);
                    ++ij;
                    block += br * bc;
                }
            });
    cls.def("extract", &Matrix::extract);
    cls.def("set_zero", &Matrix::set_zero);
    cls.def("set_identity", &Matrix::set_identity);
    cls.def("set_diagonal", &Matrix::set_diagonal);
    cls.def(
        "set_rows_to_diagonal",
        [](Matrix& self, std::vector<icus::index_type>& indices,
           value_type value) { self.set_rows_to_diagonal(indices, value); });
    cls.def("set_rows_to_identity",
            [](Matrix& self, std::vector<icus::index_type>& indices) {
                self.set_rows_to_identity(indices);
            });
    cls.def("set_rows_to_identity",
            [](Matrix& self, icus::ISet is) { self.set_rows_to_identity(is); });
    cls.def("set_rows_to_zero",
            [](Matrix& self, std::vector<icus::index_type>& indices) {
                self.set_rows_to_zero(indices);
            });
    cls.def("set_cols_to_zero",
            [](Matrix& self, std::vector<icus::index_type>& indices) {
                self.set_cols_to_zero(indices);
            });

    module.def("make_laplacian", &make_laplacian<Matrix>);
}

template <typename Vector>
void bind_vector(nb::module_& module, std::string class_name) {
    auto cls = nb::class_<Vector>(module, class_name.c_str());
    // cls.def(nb::init<>());
    cls.def(nb::init<icus::ISet&>());
    cls.def_ro("sites", &Vector::sites);
    cls.def_prop_ro("size", &Vector::size);
    cls.def_prop_ro("block_size",
                    [](Vector& self) { return Vector::block_size; });
    cls.def(
        "as_array",
        [](Vector& self) {
            return nb::ndarray<nb::numpy, loaf::value_type>(
                self.vector->data(), {self.size() * Vector::block_size},
                nb::handle());
        },
        nb::rv_policy::reference_internal);
    cls.def("dump", &Vector::dump);
    cls.def(
        "get_twin", [](Vector& self) { return Vector(self.sites); },
        "Construct an empty vector with the same type, block_size and sites "
        "attribute as self.");
}

template <size_type bsize>
void bind_all_mat_vect(nb::module_& module) {
    if constexpr (bsize > 0) {
        std::string mat_name = "OnePhysicsBlockMatrix" + std::to_string(bsize);
        bind_matrix<OnePhysicsBlockCOO<bsize>>(module, mat_name);
        std::string vect_name = "OnePhysicsVector" + std::to_string(bsize);
        bind_vector<OnePhysicsVector<bsize>>(module, vect_name);
        std::string sys_name =
            "make_block_linear_system" + std::to_string(bsize);
        module.def(sys_name.c_str(), &loaf::make_block_linear_system<bsize>);

        bind_all_mat_vect<bsize - 1>(module);
    }
}

void add_matrix_wrapper(nb::module_& module) {
    // bind_COO<ScalarCOO>(module, "COO");
    // bind block COO of size 1 to 9
    bind_all_square_block_COO<9>(module);
    // bind OnePhysicsBlockMatrix and OnePhysicsVector of size 1 to 9
    bind_all_mat_vect<9>(module);
}
