// #include <loaf/matrix.h>
#include <loaf/solve.h>
#include <nanobind/eigen/dense.h>
#include <nanobind/stl/shared_ptr.h>
#include <nanobind/stl/string.h>
#include <nanobind/stl/tuple.h>

namespace nb = nanobind;

using namespace loaf;

template <size_type bs>
static void add_methods(auto& cls) {
    if constexpr (bs > 0) {
        cls.def("compute_block", &Solver::compute<bs>);
        cls.def("solve", &Solver::solve<bs>);
        cls.def("residual", &Solver::residual<bs>);

        add_methods<bs - 1>(cls);
    }
}

void add_solver_wrapper(nb::module_& module) {
    auto cls = nb::class_<Solver>(module, "Solver");
    cls.def(nb::init<>());
    add_methods<9>(cls);
    cls.def("info", &Solver::info);
}
