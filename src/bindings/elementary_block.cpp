#include <loaf/elementary_block.h>
#include <nanobind/nanobind.h>
#include <nanobind/stl/string.h>
#include <nanobind/stl/tuple.h>

#include <sstream>

namespace nb = nanobind;
using namespace loaf;

template <typename T, typename... Ts>
void add_string_representation(nb::class_<T, Ts...>& cls) {
    cls.def("__str__", [](const T& self) {
        std::stringstream buffer;
        buffer << self;
        return buffer.str();
    });
}

void add_triplet_wrapper(nb::module_& module) {
    using triplet = COO_triplet<double>;
    auto tcls = nb::class_<triplet>(module, "Triplet");
    tcls.def(nb::init<>());
    tcls.def(nb::init<index_type, index_type, double>());
    tcls.def_ro("row", &triplet::_row);
    tcls.def_ro("column", &triplet::_column);
    tcls.def_ro("value", &triplet::_entry);
    tcls.def("as_tuple", [](triplet& self) {
        return nb::make_tuple(self._row, self._column, self._entry);
    });
    tcls.def("__str__", [](triplet& self) {
        return "Triplet(" + std::to_string(self._row) + ", " +
               std::to_string(self._column) + ", " +
               std::to_string(self._entry) + ")";
    });
}

template <loaf::size_type m, loaf::size_type n>
void add_elementary_block_wrapper(nb::module_& module) {
    auto size_str = std::to_string(m) + std::to_string(n);
    std::string eb_class_name = std::string("ElementaryBlock") + size_str;
    auto ebcls =
        nb::class_<ElementaryBlock<m, n>>(module, eb_class_name.c_str());
    ebcls.def(nb::init<>());
    ebcls.def("size",
              [](ElementaryBlock<m, n>& self) { return nb::make_tuple(m, n); });
    add_string_representation(ebcls);
}

void add_elementary_block_wrapper(nb::module_& module) {
    add_elementary_block_wrapper<1, 2>(module);
    add_elementary_block_wrapper<2, 1>(module);
    add_elementary_block_wrapper<1, 3>(module);
    add_elementary_block_wrapper<3, 1>(module);
    add_elementary_block_wrapper<2, 2>(module);
    add_elementary_block_wrapper<2, 3>(module);
    add_elementary_block_wrapper<3, 2>(module);
    add_elementary_block_wrapper<3, 3>(module);
    add_triplet_wrapper(module);
}
