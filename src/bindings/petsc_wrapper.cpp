#include <loaf/block_matrix.h>
#include <nanobind/nanobind.h>
#include <petscmat.h>
#include <petscviewer.h>

namespace nb = nanobind;
using namespace loaf;

template <typename PetscObject>
auto cast_to_PETSc(nb::object obj) {
    // cython object has a handle attribute that stores the PETSc object adress
    auto handle = nb::cast<long>(obj.attr("handle"));
    return reinterpret_cast<PetscObject>((void*)handle);
}

// FIXME: this could be elsewhere or rely on petsc4py
void dump_matrix_info(Mat& A, std::ostream& os) {
    PetscInt m, n;
    MatGetSize(A, &m, &n);
    os << "PETSc matrix: " << m << " x " << n << std::endl;
    MatInfo info;
    MatGetInfo(A, MAT_LOCAL, &info);
    std::cerr << "MatInfo: " << std::endl
              << "nz_allocated: " << info.nz_allocated << std::endl
              << "nz_used: " << info.nz_used << std::endl
              << "nz_unneeded: " << info.nz_unneeded << std::endl
              << "mallocs: " << info.mallocs << std::endl;
}

// FIXME: this could be elsewhere or rely on petsc4py
void petsc_ascii_dump(Mat& A, const std::string& filename) {
    PetscViewer viewer;
    PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename.c_str(), &viewer);
    PetscViewerFlush(viewer);
    MatView(A, viewer);
    PetscViewerDestroy(&viewer);
}

template <typename MatrixType>
void fill_PETSc_Mat(nb::object pyA, MatrixType& matrix) {
    assert(matrix.coo->is_sorted_and_unique());
    auto A = cast_to_PETSc<Mat>(pyA);
    // FIXME: is this necessary? it is probably lightweight
    MatZeroEntries(A);
    PetscInt m, n;
    for (auto&& t : matrix.coo->get_entries()) {
        for (auto&& [ij, v] : t.split()) {
            assert(std::cmp_less(std::get<0>(ij),
                                 std::numeric_limits<PetscInt>::max()));
            assert(std::cmp_less(std::get<1>(ij),
                                 std::numeric_limits<PetscInt>::max()));
            std::tie(m, n) = ij;
            // FIXME: could be improved using directly the array or
            // MatSetValuesBlocked
            //       Efficiency Alert: The routine MatSetValuesBlocked() may
            //       offer much better efficiency for users of block sparse
            //       formats (MATSEQBAIJ and MATMPIBAIJ).
            MatSetValues(A, 1, &m, 1, &n, &v, INSERT_VALUES);
        }
    }
    MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
}

template <size_type block_size = 9>
void bind_block_fillers(nb::module_& module) {
    if constexpr (block_size > 0) {
        using MatrixType = OnePhysicsBlockCOO<block_size>;
        module.def("fill_PETSc_Mat", &fill_PETSc_Mat<MatrixType>);
        bind_block_fillers<block_size - 1>(module);
    }
}

void bind_petsc_wrapper(nb::module_& module) { bind_block_fillers(module); }
