#include <nanobind/nanobind.h>

namespace nb = nanobind;

void add_matrix_wrapper(nb::module_&);
void add_elementary_block_wrapper(nb::module_&);
void add_solver_wrapper(nb::module_&);
#ifndef _WIN32
void bind_petsc_wrapper(nb::module_&);
#endif

NB_MODULE(bindings, module) {
    add_matrix_wrapper(module);
    add_elementary_block_wrapper(module);
    add_solver_wrapper(module);
#ifndef _WIN32
    bind_petsc_wrapper(module);
    module.attr("with_petsc") = true;
#else
    module.attr("with_petsc") = false;
#endif
}
