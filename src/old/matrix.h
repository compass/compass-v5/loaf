#pragma once

#include <cassert>
#include <fstream>
#include <iostream>
#include <tuple>

#include "compass_cxx_utils/pretty_print.h"
#include "elementary_block.h"
#include "icus/ISet.h"
#include "scalar_coo.h"

namespace loaf {

template <size_type Rows, size_type Cols>
struct Matrix {
    using COO = ScalarCOO;
    using coo_ptr = std::shared_ptr<COO>;
    using block_type = ElementaryBlock<Rows, Cols>;

    static constexpr size_type block_rows = Rows;
    static constexpr size_type block_cols = Cols;
    icus::ISet row_sites, col_sites;
    coo_ptr coo;

   public:
    Matrix() = default;
    Matrix(const icus::ISet &sites)
        // Using sub ISet of sites to ensure call to parent() is legal
        : row_sites{sites.extract(
              std::vector<index_type>(sites.begin(), sites.end()))},
          col_sites{sites.extract(
              std::vector<index_type>(sites.begin(), sites.end()))},
          coo(new COO(sites.size() * block_rows, sites.size() * block_cols)) {}

   private:
    Matrix(const icus::ISet rows, const icus::ISet cols, coo_ptr storage)
        : row_sites{rows}, col_sites{cols}, coo{storage} {}

   public:
    auto size() const {
        return std::make_tuple(row_sites.size(), col_sites.size());
    }
    size_type non_zeros() const {
        std::set<index_type> row_site_bag(row_sites.mapping().begin(),
                                          row_sites.mapping().end());
        std::set<index_type> col_site_bag(col_sites.mapping().begin(),
                                          col_sites.mapping().end());
        size_type nnz = 0;
        for (auto &&t : coo->get_entries()) {
            auto is_my_row =
                (row_site_bag.find(t.row() / block_rows) != row_site_bag.end());
            auto is_my_col =
                (col_site_bag.find(t.col() / block_cols) != col_site_bag.end());
            if (is_my_row && is_my_col) {
                nnz += 1;
            }
        }
        return nnz;
    }
    void set_zero() {
        std::set<index_type> row_site_bag(row_sites.mapping().begin(),
                                          row_sites.mapping().end());
        std::set<index_type> col_site_bag(col_sites.mapping().begin(),
                                          col_sites.mapping().end());
        auto is_local_entry = [row_site_bag, col_site_bag](COO::triplet t) {
            auto is_my_row =
                (row_site_bag.find(t.row() / block_rows) != row_site_bag.end());
            auto is_my_col =
                (col_site_bag.find(t.col() / block_cols) != col_site_bag.end());
            return is_my_row && is_my_col;
        };
        coo->erase_if(is_local_entry);
    }

    struct dispatcher {
        index_type I, J;
        coo_ptr coo;
        void operator+=(block_type &b) {
            for (index_type k = 0; k < block_rows; ++k) {
                for (index_type l = 0; l < block_cols; ++l) {
                    coo->add_entry(I + k, J + l, b(k, l));
                }
            }
        }
        void operator+=(block_type &&b) { *this += b; }
        void operator+=(value_type &b) { *this += block_type::Constant(b); }
        void operator+=(value_type &&b) { *this += b; }

        void operator-=(block_type &&b) { *this += -1. * b; }
        void operator-=(value_type &b) { *this += block_type::Constant(-b); }
        void operator-=(value_type &&b) { *this += -b; }
    };

    auto operator()(index_type i, index_type j) {
        auto I = row_sites.mapping()[i] * block_rows,
             J = col_sites.mapping()[j] * block_cols;
        return dispatcher(I, J, coo);
    }

    auto &operator()(index_type i, index_type j, index_type bi, index_type bj) {
        return coo->get_new(i * block_rows + bi, j * block_cols + bj);
    }

    auto extract(icus::ISet rows, icus::ISet cols) const {
        assert(rows.is_included_in(row_sites));
        assert(cols.is_included_in(col_sites));
        icus::ISet new_rows = rows.rebase(row_sites.parent());
        icus::ISet new_cols = cols.rebase(col_sites.parent());
        return Matrix<block_rows, block_cols>(new_rows, new_cols, coo);
    }

    void reset_row(const index_type i) {
        assert(i < row_sites.size());
        coo->zero_row(row_sites.mapping(i));
    }

    void reset_col(index_type j) const {
        assert(j < col_sites.size());
        index_type J = col_sites.mapping(j);
        coo->zero_col(col_sites.mapping(j));
    }

    // std::vector<typename COO::triplet> get_row(index_type i) const {
    //     assert(i < row_sites.size());
    //     index_type I = row_sites.mapping(i);
    //     std::vector<typename COO::triplet> row{};
    //     row.reserve(30 * block_rows * block_cols);
    //     for (auto &&k = 0; k < block_rows; ++k) {
    //         auto scalar_row = coo->get_row(I * block_rows + k);
    //         row.insert(row.end(), scalar_row.begin(), scalar_row.end());
    //     }
    //     row.shrink_to_fit();
    //     return row;
    // }
    // std::vector<typename COO::triplet> get_col(index_type j) const {
    //     assert(j < col_sites.size());
    //     index_type J = col_sites.mapping(j);
    //     std::vector<typename COO::triplet> col{};
    //     col.reserve(30 * block_rows * block_cols);
    //     for (auto &&k = 0; k < block_cols; ++k) {
    //         auto scalar_col = coo->get_col(J * block_cols + k);
    //         col.insert(col.end(), scalar_col.begin(), scalar_col.end());
    //     }
    //     col.shrink_to_fit();
    //     return col;
    // }

    void print(std::ostream &os = std::cout) {
        os << "One physics matrix of size: " << row_sites.size() << " x "
           << col_sites.size() << std::endl;
        os << "Block size: (" << block_rows << ", " << block_cols << ")"
           << std::endl;
        os << "Underlying storage:" << std::endl;
        compass::utils::Pretty_printer{os, "", "\n", 9, -1, "\n"}(*coo);
    }
    friend std::ostream &operator<<(
        std::ostream &os, const Matrix<block_rows, block_cols> &matrix) {
        os << "One physics matrix of size: " << matrix.row_sites.size() << " x "
           << matrix.col_sites.size() << std::endl;
        os << "Block size: (" << block_rows << ", " << block_cols << ")"
           << std::endl;
        os << "Underlying storage:" << std::endl;
        os << *matrix.coo;
        return os;
    }
    void dump(const std::string filename) const {
        std::ofstream dumpfile;
        dumpfile.open(filename);
        dumpfile.precision(12);
        dumpfile << *this;
        dumpfile.close();
    }
    // FIXME: this might be a duplicate of dump and or operator<<
    void petsc_ascci_dump(const std::string &filename) {
        if (!coo->is_sorted_and_unique()) {
            std::cerr << "WARNING: matrix is not sorted" << std::endl;
        }
        std::vector<std::vector<std::pair<int, double>>> tmp;
        tmp.resize(block_rows * row_sites.size());
        int i, j;
        for (auto &&t : coo->get_entries()) {
            for (auto &&[ij, v] : t.split()) {
                std::tie(i, j) = ij;
                if (v != 0) {
                    tmp[i].emplace_back(j, v);
                }
            }
        }
        std::ofstream f{filename};
        f << "LOAF matrix" << std::endl
          << "custom petsc like output" << std::endl;
        i = 0;
        for (auto &&row : tmp) {
            f << "row " << i << ":";
            for (auto &&[j, x] : row) {
                f << " (" << j << ", " << x << ") ";
            }
            f << std::endl;
            ++i;
        }
        f.close();
    }
};

template <size_type block_size>
using OnePhysicsCOO = Matrix<block_size, block_size>;

}  // namespace loaf
