#pragma once

#include <cassert>
#include <iostream>
#include <tuple>
#include <vector>

#include "elementary_block.h"

namespace loaf {

struct ScalarCOO {
    // Holds a sequence of (i, j, entry) triplets.
    // there can be several occurrences of the same (i, j)

    using triplet = COO_triplet<value_type>;

    size_type nrows = 0, ncols = 0;
    std::vector<triplet> entries;

   public:
    ScalarCOO() = default;
    ScalarCOO(const ScalarCOO &) = default;
    ScalarCOO(const size_type &M, const size_type &N) : nrows{M}, ncols{N} {
        assert(M > 0);
        assert(N > 0);
        entries.reserve(10 * M);
    }

    auto size() const { return std::make_tuple(nrows, ncols); }
    const auto &get_entries() const { return entries; }
    auto begin() const { return entries.begin(); }
    auto end() const { return entries.end(); }
    size_type non_zeros() const { return entries.size(); }
    value_type &operator()(index_type row, index_type col) {
        return get_new(row, col);
    }

    bool is_constructed() const { return (nrows > 0 && ncols > 0); }
    bool is_set_up() const {
        // Check if values are sorted and unique
        assert(is_constructed());
        auto prev = entries.begin();
        auto next = entries.begin() + 1;
        while (next != entries.end()) {
            if (*next <= *prev) {
                return false;
            }
            ++next;
            ++prev;
        }
        return true;
    }

    void add_entry(index_type row, index_type col, const value_type &val) {
        // FIXME: Dealing with all cases of parameters being either rvalues or
        // lvalue references requires a lot of overloads. Using copy requires a
        // single implementation
        assert(is_constructed());
        assert(row >= 0 && row < nrows);
        assert(col >= 0 && col < ncols);
        entries.emplace_back(row, col, val);
    }

    value_type &get_new(index_type i, index_type j) {
        // Append a new zero entry at end of container
        // and return a reference to it.
        assert(is_constructed());
        assert(i >= 0 && i < nrows);
        assert(j >= 0 && j < ncols);
        entries.emplace_back(i, j, 0.);
        return entries.back().value_ref();
    }

    auto check(const index_type i, index_type j) {
        for (auto &&t : entries) {
            auto it = t.row();
            auto jt = t.col();
            if (it == i && jt == j) {
                std::cerr << "(" << i << "," << j << "): " << t.value()
                          << std::endl;
            }
        }
    }

    value_type &get_last() {
        // Return a reference to the last
        // exisiting entry of the container
        assert(is_constructed());
        return entries.back().value_ref();
    }

    void sort_and_add_entries() {
        // Sort entries in lexical order
        assert(is_constructed());
        auto comp = [this](const triplet &prev, const triplet &next) {
            return (prev.row() * ncols + prev.col()) <
                   (next.row() * ncols + next.col());
        };

        if (entries.empty()) return;

        std::sort(entries.begin(), entries.end(), comp);

        // in place compression
        // cf. Eigen::makeCompressed()
        auto p = entries.begin();
        auto q = p;
        for (++p; p != entries.end(); ++p) {
            assert(*q <= *p);
            if (*q < *p) {
                ++q;
                *q = *p;
                assert(*q == *p);
            } else {
                assert(*q == *p);
                q->_entry += p->_entry;
            }
        }
        entries.erase(++q, entries.end());
    }

    bool is_sorted_and_unique() const {
        if (entries.empty()) return true;
        auto p = entries.begin();
        for (++p; p != entries.end(); ++p) {
            if (!(*prev(p) < *p)) return false;
        }
        return true;
    }

    // FIXME: this assumes sort_and_add_entries has been called
    auto nnz_per_row() const {
        // FIXME: assert(is_sorted_and_unique());
        std::vector<int> result;
        result.resize(nrows);
        assert(std::all_of(result.begin(), result.end(),
                           [](auto &&x) { return x == 0; }));
        for (auto &&t : entries) {
            assert(t.row() >= 0 && t.row() < result.size());
            ++result[t.row()];
        }
        return result;
    }

    void reset() { entries.clear(); }

    void zero_row(const auto i) {
        for (auto &&t : entries) {
            if (t.row() == i) t.value_ref() = 0;
        }
    }

    void zero_col(const auto j) {
        for (auto &&t : entries) {
            if (t.col() == j) t.value_ref() = 0;
        }
    }

    template <typename F>
    void erase_if(F &&condition) {
        std::erase_if(entries, std::forward<F>(condition));
    }

    template <typename F>
    void erase_if_contiguous(F &&condition) {
        // Erases all entries for which condition is true,
        // assuming they are stored as a contiguous chunk
        auto p = entries.begin();
        while (p != entries.end() && !condition(*p)) ++p;
        auto q = p;
        while (q != entries.end() && condition(*q)) ++q;
        assert(std::none_of(q, entries.end(), condition));
        entries.erase(p, q);
    }

    std::vector<triplet> get_row(index_type i) const {
        std::vector<triplet> row{};
        row.reserve(30);  // Upper bound on the number of non zeros
        for (auto &&t : entries) {
            if (t.row() == i) {
                row.push_back(t);
            }
        }
        row.shrink_to_fit();
        return row;
    }

    std::vector<triplet> get_col(index_type j) const {
        std::vector<triplet> col{};
        col.reserve(30);  // Upper bound on the number of non zeros
        for (auto &&t : entries) {
            if (t.col() == j) {
                col.push_back(t);
            }
        }
        col.shrink_to_fit();
        return col;
    }

    friend std::ostream &operator<<(std::ostream &os, const ScalarCOO &coo) {
        os << "COO matrix of size: (" << std::get<0>(coo.size()) << ", "
           << std::get<1>(coo.size()) << ")\n";
        os << "Entries set: " << coo.entries.size();
        os << std::endl;
        for (auto &&t : coo.entries) {
            os << t << std::endl;
        }
        return os;
    }
};
}  // namespace loaf
