from .bindings import *

# from .factories import make_block_linear_system, make_linear_system


def as_matrix(v):
    return v.as_array().reshape(v.size, v.block_size)


OnePhysicsVector1.as_matrix = as_matrix
OnePhysicsVector2.as_matrix = as_matrix
OnePhysicsVector3.as_matrix = as_matrix
OnePhysicsVector4.as_matrix = as_matrix
OnePhysicsVector5.as_matrix = as_matrix
OnePhysicsVector6.as_matrix = as_matrix
OnePhysicsVector7.as_matrix = as_matrix
OnePhysicsVector8.as_matrix = as_matrix
OnePhysicsVector9.as_matrix = as_matrix
