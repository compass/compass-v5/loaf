#pragma once

#include <icus/types.h>

namespace loaf {

using index_type = icus::index_type;
using size_type = icus::size_type;

using icus::size_to_index;

}  // namespace loaf
