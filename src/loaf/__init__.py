import verstr

try:
    from . import _version

    __version__ = verstr.verstr(_version.version)
except ImportError:
    __version__ = None

# all bindings are defined in a single compiled module
# this is not the only possible strategy...
# adapt the current file to your needs

import compass

with compass.nanobind():
    from loaf.bindings import *

# Prevent import from root file, because specializations
# for the Solver class are made in the solver module
del Solver

# FIXME: this is mandatory to have v.as_matrix... should be elsewhere?
from loaf import vector
from loaf.factories import *
