#pragma once

#include <cassert>
#include <iostream>

#include "compass_cxx_utils/pretty_print.h"
#include "elementary_block.h"
#include "icus/ISet.h"

namespace loaf {
template <size_type bsize>
struct OnePhysicsVector {
    using block_type = ElementaryColBlock<bsize>;
    using vector_type = Eigen::Matrix<value_type, Eigen::Dynamic, 1>;
    using vector_ptr = std::shared_ptr<vector_type>;

    static constexpr auto block_size = bsize;
    vector_ptr vector;
    icus::ISet sites;
    icus::ISet::Mapping_view sites_map;

    // OnePhysicsVector() = default;
    OnePhysicsVector(const icus::ISet &s)
        : sites{s.extract(std::vector<index_type>(s.begin(), s.end()))},
          sites_map{sites.mapping_view()},
          vector{new vector_type()} {
        assert(sites.has_base());
        *vector = vector_type::Zero(sites.size() * block_size);
    }
    OnePhysicsVector(const OnePhysicsVector &other)
        : sites{other.sites},
          sites_map{sites.mapping_view()},
          vector{other.vector} {
        assert(sites_map == other.sites_map);
    }

   private:
    OnePhysicsVector(const icus::ISet &s, vector_ptr v)
        : sites{s}, sites_map{sites.mapping_view()}, vector{v} {
        assert(sites.has_base());
    }

   public:
    auto extract(const icus::ISet &row_sites) {
        assert(!row_sites.empty());
        assert(row_sites.is_included_in(sites));
        icus::ISet new_sites(row_sites.rebase(sites.parent()));
        return OnePhysicsVector<block_size>{new_sites, vector};
    }
    size_type size() const { return sites.size(); }
    // void set_zero() { vector->setZero(); }
    auto wrap(const vector_ptr &v) const { return OnePhysicsVector{sites, v}; }
    void operator=(block_type &block) {
        for (auto &&k : sites.mapping()) {
            vector->block<block_size, 1>(k * block_size, 0) = block;
        }
    };
    void operator=(block_type &&block) { *this = block; }
    void operator=(value_type &val) { *this = block_type::Constant(val); }
    void operator=(value_type &&val) { *this = val; }
    auto operator()(const index_type i) {
        return vector->block<block_size, 1>(sites_map[i] * block_size, 0);
    }
    auto operator()(const index_type i) const {
        return vector->block<block_size, 1>(sites_map[i] * block_size, 0);
    }
    friend std::ostream &operator<<(std::ostream &os,
                                    const OnePhysicsVector<block_size> &v) {
        os << "OnePhysicsVector of size: " << v.size() << std::endl;
        os << "Block size: " << block_size << std::endl;
        os << *v.vector << std::endl;
        return os;
    }
    void print(std::ostream &os = std::cout) const {
        os << "OnePhysicsVector of size: " << size() << std::endl;
        os << "Block size: " << block_size << std::endl;
        compass::utils::pretty_print(*vector, os);
    }
    void dump(const std::string filename) const {
        std::ofstream dumpfile;
        dumpfile.open(filename);
        dumpfile.precision(12);
        dumpfile << *this;
        dumpfile.close();
    }
};
}  // namespace loaf
