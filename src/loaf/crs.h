#pragma once
#include <tuple>
#include <vector>

#include "elementary_block.h"

namespace loaf {

template <std::size_t m, std::size_t n>
struct ElementaryBlockCRS {
    using entry_type = ElementaryBlock<m, n>;

    std::vector<size_type> offsets;
    std::vector<index_type> columns;
    std::vector<entry_type> values;
    size_type nrows, ncols;

    ElementaryBlockCRS() = default;
    ElementaryBlockCRS(const ElementaryBlockCRS<m, n> &) = default;

    bool is_constructed() { return (nrows > 0 && ncols > 0); }
    auto size() const { return std::make_tuple(nrows, ncols); }

    friend std::ostream &operator<<(std::ostream &os,
                                    const ElementaryBlockCRS<m, n> &crs) {
        os << "ElementaryBlockCRS matrix of size: (" << std::get<0>(crs.size())
           << ", " << std::get<1>(crs.size()) << ")";
        return os;
    }
};

}  // namespace loaf
