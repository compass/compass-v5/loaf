from . import bindings
from .solver import LinearSolverStatus
from compass_utils.exceptions import LinearSolverFailure


class EigenSolver:
    def __init__(self):
        self._solver = bindings.Solver()

    def solve(self, vector):
        x = self._solver.solve(vector)
        success, message = self._solver.info()
        if not success:
            raise LinearSolverFailure(message)
        return x, LinearSolverStatus(solve_success=success, message=message)

    def set_up(self, matrix):
        return self._solver.compute_block(matrix)

    def residual(self, vector):
        return self._solver.residual(vector)
