#pragma once
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include <tuple>

#include "loaf/block_matrix.h"
#include "loaf/elementary_block.h"
// #include "loaf/matrix.h"
#include "loaf/vector.h"

namespace loaf {

struct Solver {
    using matrix_type = Eigen::SparseMatrix<loaf::value_type>;
    using solver_type =
        Eigen::SparseLU<matrix_type, Eigen::COLAMDOrdering<int>>;
    using vector_type = Eigen::VectorXd;
    using vector_ptr = std::shared_ptr<vector_type>;

    matrix_type matrix;
    solver_type solver;
    vector_ptr solution;

    Solver() : solution{vector_ptr(new vector_type())} {};
    // template <size_type bs>
    // void compute(const OnePhysicsCOO<bs> &J) {
    //     assert(J.row_sites.size() == J.col_sites.size());
    //     matrix.resize(J.row_sites.size() * bs, J.col_sites.size() * bs);
    //     solution->resize(J.col_sites.size() * bs);
    //     // Fill sparse matrix from an array of (i, j, value) triplets
    //     matrix.setFromTriplets(J.coo->get_entries().begin(),
    //                            J.coo->get_entries().end());
    //     // Eliminate values that are too small to be meaningful
    //     // matrix.prune([](auto i, auto j, auto value) {
    //     //     return (std::abs(value) > 1.e-12);
    //     // });
    //     // Compression avoids copies in the factorization process
    //     matrix.makeCompressed();
    //     // Compute the numerical factorization
    //     solver.compute(matrix);
    // }

    // FIXME: this should be elsewhere
    void dump_eigen_matrix_to_petsc_ascii(const std::string &filename) {
        std::ofstream f{filename};
        f << "Eigen matrix" << std::endl
          << "custom petsc like output" << std::endl;
        for (Eigen::Index i = 0; i < matrix.rows(); ++i) {
            f << "row " << i << ":";
            for (Eigen::Index j = 0; j < matrix.cols(); ++j) {
                auto x = matrix.coeff(i, j);
                if (x != 0) {
                    f << " (" << j << ", " << x << ") ";
                }
            }
            f << std::endl;
        }
        f.close();
    }

    template <size_type bs>
    void compute(const OnePhysicsBlockCOO<bs> &J) {
        assert(J.row_sites.size() == J.col_sites.size());
        matrix.resize(J.row_sites.size() * bs, J.col_sites.size() * bs);
        solution->resize(J.col_sites.size() * bs);
        std::vector<COO_triplet<double>> flattened;
        flattened.reserve(J.row_sites.size() * bs * bs);
        index_type k = 0;
        // FIXME: should be factorized between solvers, a method of its own
        constexpr auto bsi = size_to_index(bs);
        for (auto &&block_triplet : J.coo->get_entries()) {
            auto i = block_triplet.row(), j = block_triplet.col();
            for (index_type k = 0; k < bs; ++k) {
                for (index_type l = 0; l < bs; ++l) {
                    flattened.emplace_back(i * bsi + k, j * bsi + l,
                                           block_triplet.value()(k, l));
                }
            }
        }
        // FIXME: could we avoid the copy into flattened_array ?
        // Fill sparse matrix from an array of (i, j, value) triplets
        matrix.setFromTriplets(flattened.begin(), flattened.end());
        // Eliminate values that are too small to be meaningful
        // matrix.prune([](auto i, auto j, auto value) {
        //     return (std::abs(value) > 1.e-12);
        // });
        // Compression avoids copies in the factorization process
        // FIXME: the result of setFromTriplets is ALREADY compressed
        //        cf.
        //        https://eigen.tuxfamily.org/dox/classEigen_1_1SparseMatrix.html#title35
        matrix.makeCompressed();
        // Compute the numerical factorization
        solver.compute(matrix);
    }

    template <size_type bs>
    OnePhysicsVector<bs> solve(const OnePhysicsVector<bs> &RHS) {
        *solution = solver.solve(*RHS.vector);
        return RHS.wrap(solution);
    }

    auto info() const {
        bool success;
        std::string message;
        if (solver.info() == Eigen::NumericalIssue) {
            message = "Eigen::NumericalIssue: Problem in LU factorization";
            success = false;
        } else if (solver.info() == Eigen::InvalidInput) {
            message = "Eigen::InvalidInput: Invalid input matrix";
            success = false;
        } else if (solver.info() == Eigen::Success) {
            message = "Eigen::Success: Solve succeeded";
            success = true;
        } else {
            message = "";
            success = false;
        }
        return std::make_tuple(success, message);
    }

    // not used anymore
    // template <size_type bs>
    // OnePhysicsVector<bs> get_solution_as(
    //     const OnePhysicsVector<bs> other) const {
    //     OnePhysicsVector<bs> x(other.sites);
    //     x.vector = solution;
    //     return x;
    // }

    template <size_type bs>
    double residual(const OnePhysicsVector<bs> &RHS) const {
        return (*RHS.vector - matrix * (*solution)).norm();
    }
};

}  // namespace loaf
