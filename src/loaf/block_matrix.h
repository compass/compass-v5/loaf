#pragma once

#include <cassert>
#include <fstream>
#include <iostream>
#include <memory>

#include "block_coo.h"
#include "compass_cxx_utils/pretty_print.h"
#include "elementary_block.h"
#include "icus/ISet.h"

namespace loaf {

template <size_type BlockRows, size_type BlockCols>
struct BlockMatrix {
    using COO = ElementaryBlockCOO<BlockRows, BlockCols>;
    using coo_ptr = std::shared_ptr<COO>;
    using block_type = ElementaryBlock<BlockRows, BlockCols>;

    static constexpr size_type block_rows = BlockRows;
    static constexpr size_type block_cols = BlockCols;
    icus::ISet row_sites, col_sites;
    icus::ISet::Mapping_view row_map, col_map;
    coo_ptr coo;

   public:
    // BlockMatrix() = default;
    BlockMatrix(const icus::ISet &sites)
        // Using sub ISet of sites to ensure call to parent() is legal
        // FIXME: possible performance penalty while ISet mapping is a vector
        // FIXME: icus self-extraction ?
        : row_sites{sites.rebase(sites)},
          col_sites{sites.rebase(sites)},
          row_map{row_sites.mapping_view()},
          col_map{col_sites.mapping_view()},
          coo(new COO(sites.size(), sites.size())) {}
    BlockMatrix(const BlockMatrix &other)
        : row_sites{other.row_sites},
          col_sites{other.col_sites},
          row_map{row_sites.mapping_view()},
          col_map{col_sites.mapping_view()},
          coo{other.coo} {
        assert(row_map == other.row_map);
        assert(col_map == other.col_map);
    }

   private:
    BlockMatrix(const icus::ISet rows, const icus::ISet cols, coo_ptr storage)
        : row_sites{rows},
          col_sites{cols},
          row_map{row_sites.mapping_view()},
          col_map{col_sites.mapping_view()},
          coo{storage} {}

   public:
    const auto size() const {
        return std::make_tuple(row_sites.size(), col_sites.size());
    }
    const size_type non_zeros() const {
        std::set<index_type> row_site_bag(row_sites.mapping().begin(),
                                          row_sites.mapping().end());
        std::set<index_type> col_site_bag(col_sites.mapping().begin(),
                                          col_sites.mapping().end());
        size_type nnz = 0;
        for (auto &&t : coo->get_entries()) {
            auto is_my_row = (row_site_bag.find(t.row()) != row_site_bag.end());
            auto is_my_col = (col_site_bag.find(t.col()) != col_site_bag.end());
            if (is_my_row && is_my_col) {
                nnz += 1;
            }
        }
        return nnz;
    }
    auto &operator()(const index_type i, const index_type j) {
        return coo->get_new(row_map[i], col_map[j]);
    }

    // FIXME: the most important function coo is a pointer, hence no copies
    // involved
    auto extract(icus::ISet rows, icus::ISet cols) {
        assert(!row_sites.empty());
        assert(!col_sites.empty());
        assert(rows.is_included_in(row_sites));
        assert(cols.is_included_in(col_sites));
        icus::ISet new_rows = rows.rebase(row_sites.parent());
        icus::ISet new_cols = cols.rebase(col_sites.parent());
        return BlockMatrix<block_rows, block_cols>(new_rows, new_cols, coo);
    }

    auto lambda_cond_from_bags(auto const &row_site_bag,
                               auto const &col_site_bag) {
        return [row_site_bag, col_site_bag](COO::triplet t) {
            auto is_my_row = (row_site_bag.find(t.row()) != row_site_bag.end());
            auto is_my_col = (col_site_bag.find(t.col()) != col_site_bag.end());
            return is_my_row && is_my_col;
        };
    }

    // nullify the matrix, depend if it is the whole matrix or an extraction
    void set_zero() {
        assert(!row_sites.parent().has_base());
        assert(!col_sites.parent().has_base());
        // test if the rows/columns have been extracted (sub iset of sites)
        // same size is enough: the mapping can be distinct from identity (other
        // numbering) set_zero will concern the whole global matrix
        if (row_sites.size() == row_sites.parent().size() &&
            col_sites.size() == col_sites.parent().size()) {
            // FIXME: reset is a bad choice:
            //        coo.reset() nullify the coo_ptr
            //        whereas coo->reset() clear the container which pointed to
            //        by coo_ptr
            coo->reset();
        } else {
            // not optimal, row OR col only can be extracted
            std::set<index_type> row_site_bag(row_sites.mapping().begin(),
                                              row_sites.mapping().end());
            std::set<index_type> col_site_bag(col_sites.mapping().begin(),
                                              col_sites.mapping().end());
            // FIXME: use i in row_sites and j in row_sites
            coo->set_zero_if(lambda_cond_from_bags(row_site_bag, col_site_bag));
        }
    }

    void set_cols_to_zero(const auto &cols) {
        // mapping from extracted indices to global matrix indices
        std::set<index_type> col_site_bag;
        for (auto &&j : cols) {
            assert(j < col_sites.size());
            col_site_bag.insert(col_map[j]);
        }
        // warning: do not erase rows outside of row_sites
        // todo: this test is not enough: can have same size and other numbering
        if (row_sites.size() == row_sites.parent().size()) {
            auto is_local_entry = [col_site_bag](COO::triplet t) {
                return col_site_bag.find(t.col()) != col_site_bag.end();
            };
            coo->set_zero_if(is_local_entry);
        } else {
            std::set<index_type> row_site_bag(row_sites.mapping().begin(),
                                              row_sites.mapping().end());
            // FIXME: use i in row_sites and j in row_sites
            coo->set_zero_if(lambda_cond_from_bags(row_site_bag, col_site_bag));
        }
    }

    void set_rows_to_zero(const auto &rows) {
        // mapping from extracted indices to global matrix indices
        std::set<index_type> row_site_bag;
        for (auto &&i : rows) {
            assert(i < row_sites.size());
            row_site_bag.insert(row_map[i]);
        }
        // warning: do not erase column outside of col_sites
        // todo: this test is not enough: can have same size and other numbering
        if (col_sites.size() == col_sites.parent().size()) {
            auto is_local_entry = [row_site_bag](COO::triplet t) {
                return row_site_bag.find(t.row()) != row_site_bag.end();
            };
            coo->set_zero_if(is_local_entry);
        } else {
            std::set<index_type> col_site_bag(col_sites.mapping().begin(),
                                              col_sites.mapping().end());
            // FIXME: use i in row_sites and j in row_sites
            coo->set_zero_if(lambda_cond_from_bags(row_site_bag, col_site_bag));
        }
    }

    template <int l = -1>
    // from local indices, modify the rows to set zero outside the local
    // diagonal and value on the diagonal (if the diagonal is in the matrix)
    void set_rows_to_diagonal(const auto &rows, value_type value) {
        // mapping from extracted indices to global matrix indices
        std::set<std::pair<index_type, index_type>> diag_indices;
        std::set<index_type> row_site_bag;
        for (auto i : rows) {
            assert(i < row_sites.size());
            row_site_bag.insert(row_map[i]);
            // store the diagonal indices in its global indices
            diag_indices.insert({row_map[i], col_map[i]});
        }
        // warning: do not erase column outside of col_sites
        // todo: this test is not enough: can have same size and other numbering
        if (col_sites.size() == col_sites.parent().size()) {
            auto is_local_entry = [row_site_bag](COO::triplet t) {
                return row_site_bag.find(t.row()) != row_site_bag.end();
            };
            coo->template set_diagonal<l>(is_local_entry, diag_indices, value);
        } else {
            // the matrix is an extraction in columns
            std::set<index_type> col_site_bag(col_sites.mapping().begin(),
                                              col_sites.mapping().end());
            coo->template set_diagonal<l>(
                lambda_cond_from_bags(row_site_bag, col_site_bag), diag_indices,
                value);
        }
    }

    template <int l = -1>
    // Modify via an iset even if the Jacobian is always accessed via the index
    // in its sites indices
    void set_rows_to_diagonal(icus::ISet rows, value_type value) {
        assert(rows.is_included_in(row_sites));
        // create the list of indices in row_sites basis
        auto rrows = rows.rebase(row_sites).mapping();
        set_rows_to_diagonal<l>(rrows, value);
    }

    template <int l = -1>
    // Set zero outside the local diagonal and "value" on the diagonal
    // (if the diagonal is in the matrix)
    void set_diagonal(value_type value) {
        set_rows_to_diagonal<l>(
            std::views::iota((std::size_t)0, row_sites.size()), value);
    }

    template <int l = -1>
    void set_rows_to_identity(const auto &rows) {
        set_rows_to_diagonal<l>(rows, 1.0);
    }
    template <int l = -1>
    void set_identity() {
        set_diagonal<l>(1.0);
    }

    void sort_and_add_entries() { coo->sort_and_add_entries(); }
    bool is_sorted_and_unique() { return coo->is_sorted_and_unique(); }
    // careful, the algo is very costly
    // test is done over the whole matrix, not adapted to extracted matrix
    bool is_symetric(double eps = 1.e-10) {
        // todo: this test is not enough: can have same size and other numbering
        if (col_sites.size() != col_sites.parent().size() ||
            row_sites.size() != row_sites.parent().size())
            throw std::runtime_error(
                "is_symetric not implemented over extracted matrix");
        return coo->is_symetric(eps);
    }

    // auto get_row(index_type i) const {
    //     assert(i < row_sites.size());
    //     return coo->get_row(row_sites.mapping(i));
    // }

    // auto get_col(index_type j) const {
    //     assert(j < col_sites.size());
    //     return coo->get_col(col_sites.mapping(j));
    // }

    void print(std::ostream &os = std::cout) const {
        os << "One physics block matrix of size: " << row_sites.size() << " x "
           << col_sites.size() << std::endl;
        os << "Block size: (" << block_rows << ", " << block_cols << ")"
           << std::endl;
        os << "Underlying storage:" << std::endl;
        compass::utils::Pretty_printer{os, "\n", "\n", 6, -1, "\n"}(*coo);
    }
    friend std::ostream &operator<<(
        std::ostream &os, const BlockMatrix<block_rows, block_cols> &matrix) {
        os << "One physics block matrix of sizes: " << matrix.row_sites.size()
           << " x " << matrix.col_sites.size() << std::endl;
        os << "Block size: (" << block_rows << ", " << block_cols << ")"
           << std::endl;
        os << "Underlying storage:" << std::endl;
        os << *matrix.coo;
        return os;
    }
    void dump(std::string filename) const {
        std::ofstream dumpfile;
        dumpfile.open(filename);
        dumpfile.precision(12);
        dumpfile << *this;
        dumpfile.close();
    }
};

template <size_type BlockSize>
using OnePhysicsBlockCOO = BlockMatrix<BlockSize, BlockSize>;
}  // namespace loaf
