from loaf import (
    make_block_linear_system1,
    make_block_linear_system2,
    make_block_linear_system3,
    make_block_linear_system4,
    make_block_linear_system5,
    make_block_linear_system6,
    make_block_linear_system7,
    make_block_linear_system8,
    make_block_linear_system9,
    OnePhysicsVector1,
    OnePhysicsVector2,
    OnePhysicsVector3,
    OnePhysicsVector4,
    OnePhysicsVector5,
    OnePhysicsVector6,
    OnePhysicsVector7,
    OnePhysicsVector8,
    OnePhysicsVector9,
)


def make_block_linear_system(iset, block_size):
    if block_size == 1:
        return make_block_linear_system1(iset)
    elif block_size == 2:
        return make_block_linear_system2(iset)
    elif block_size == 3:
        return make_block_linear_system3(iset)
    elif block_size == 4:
        return make_block_linear_system4(iset)
    elif block_size == 5:
        return make_block_linear_system5(iset)
    elif block_size == 6:
        return make_block_linear_system6(iset)
    elif block_size == 7:
        return make_block_linear_system7(iset)
    elif block_size == 8:
        return make_block_linear_system8(iset)
    elif block_size == 9:
        return make_block_linear_system9(iset)
    else:
        print("Block linear system factory: Block size", block_size, "not supported")


def make_OnePhysicsVector(iset, block_size):
    if block_size == 1:
        return OnePhysicsVector1(iset)
    elif block_size == 2:
        return OnePhysicsVector2(iset)
    elif block_size == 3:
        return OnePhysicsVector3(iset)
    elif block_size == 4:
        return OnePhysicsVector4(iset)
    elif block_size == 5:
        return OnePhysicsVector5(iset)
    elif block_size == 6:
        return OnePhysicsVector6(iset)
    elif block_size == 7:
        return OnePhysicsVector7(iset)
    elif block_size == 8:
        return OnePhysicsVector8(iset)
    elif block_size == 9:
        return OnePhysicsVector9(iset)
    else:
        print("make_OnePhysicsVector: Block size", block_size, "not supported")
