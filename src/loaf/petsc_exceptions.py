from compass_utils.exceptions import LinearSolverFailure
from petsc4py import PETSc

_petsc_possible_failure_reasons = [
    name for name in dir(PETSc.KSP.ConvergedReason) if not name.startswith("__")
]


def explain_reason(reason):

    for name in _petsc_possible_failure_reasons:
        if getattr(PETSc.KSP.ConvergedReason, name) == reason:
            return name
    return f"Unknown solver failure reason: {reason}"


class InvalidConfigError(Exception):
    def __init__(self, config):
        self.config = config


class DirectSolverFailure(LinearSolverFailure):
    def __str__(self):
        return f"Direct solver failed with reason : {explain_reason(self.reason)}"


class IterativeSolverFailure(LinearSolverFailure):
    def __init__(self, reason, nit):
        super().__init__(reason)
        self.nit = nit

    def __str__(self):
        return f"Iterative solver failed after {self.nit} iterations with reason: {explain_reason(self.reason)}"
