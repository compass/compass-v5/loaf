#pragma once
#include <cassert>
#include <iostream>
#include <set>
#include <tuple>
#include <vector>

#include "elementary_block.h"

namespace loaf {

// FIXME: matrix_shape type ?
template <size_type m, size_type n>
struct ElementaryBlockCOO {
    // Holds a sequence of (i, j, entry) triplets.
    // there can be several occurrences of the same (i, j)
    // entry is an ElementaryBlock.

    using entry_type = ElementaryBlock<m, n>;
    using triplet = COO_triplet<entry_type>;

   private:
    size_type nrows = 0, ncols = 0;
    std::vector<triplet> entries;

   public:
    ElementaryBlockCOO() = default;
    ElementaryBlockCOO(const ElementaryBlockCOO<m, n> &) = default;
    ElementaryBlockCOO(const size_type &M, const size_type &N)
        : nrows{M}, ncols{N} {
        assert(M > 0);
        assert(N > 0);
        entries.reserve(10 * M);  // FIXME
    }

    auto begin() const { return entries.begin(); }
    auto end() const { return entries.end(); }
    auto size() const { return std::make_tuple(nrows, ncols); }  // FIXME: shape
    size_type non_zeros() {
        return entries.size();
    }  // FIXME: nb entries >= non_zeros
    void reset() { entries.clear(); }
    const auto &get_entries() const { return entries; }
    bool is_constructed() const {
        return (nrows > 0 && ncols > 0);
    }  // FIXME: is_empty ?
    bool is_set_up() const {  // FIXME: check sur locations
        // Check if values are sorted and unique
        assert(is_constructed());
        if (entries.size() == 0) {
            return false;
        };
        auto prev = begin();
        auto next = begin() + 1;
        while (next != end()) {
            if (*next <= *prev) {
                return false;
            }
            ++next;
            ++prev;
        }
        return true;
    }

    auto check(const index_type i,
               index_type j) {  // FIXME: check -> debug / dump
        for (auto &&t : entries) {
            auto it = t.row() * m;
            auto jt = t.col() * n;
            for (index_type k = 0; k < m; ++k) {
                for (index_type l = 0; l < n; ++l) {
                    if (it + k == i && jt + l == j) {
                        std::cerr << "(" << i << "," << j
                                  << "): " << t.value()(k, l) << std::endl;
                    }
                }
            }
        }
    }

    template <typename Entry>
    void add_entry(index_type row, index_type col, Entry &&val) {
        // FIXME: Dealing with all cases of parameters being either rvalues or
        // lvalue references requires a lot of overloads. Using copy requires a
        // single implementation
        assert(is_constructed());  // FIXME: redundant with 2 following tests
        assert(row >= 0 && row < nrows);
        assert(col >= 0 && col < ncols);
        entries.emplace_back(row, col, std::forward<Entry>(val));
    }

    entry_type &get_new(index_type i, index_type j) {
        // Append a new zero entry at end of container
        // and return a reference to it.
        assert(is_constructed());
        assert(i >= 0 && i < nrows);
        assert(j >= 0 && j < ncols);
        entries.emplace_back(i, j, entry_type::Zero());
        return entries.back().value_ref();
    }

    entry_type &get_last() {
        // Return a reference to the last
        // existing entry of the container
        assert(is_constructed());
        return entries.back().value_ref();
    }

    void sort_and_add_entries() {  // FIXME: compress ? (cf. Eigen)
        // Sort entries in lexical order
        assert(is_constructed());
        auto comp = [this](const triplet &prev, const triplet &next) {
            return (prev.row() * ncols + prev.col()) <
                   (next.row() * ncols + next.col());
        };

        std::sort(entries.begin(), entries.end(), comp);
        // in place compression
        auto p = entries.begin();
        auto q = p;
        for (++p; p != entries.end(); ++p) {
            assert(*q <= *p);
            if (*q < *p) {
                ++q;
                *q = std::move(*p);
                assert(*q == *p);
            } else {
                assert(*q == *p);
                q->_entry += std::move(p->_entry);
            }
        }
        entries.erase(++q, entries.end());
    }

    bool is_sorted_and_unique() const {
        if (entries.empty()) return true;
        auto p = entries.begin();
        for (++p; p != entries.end(); ++p) {
            if (!(*prev(p) < *p)) return false;
        }
        return true;
    }

    // FIXME: this assumes sort_and_add_entries has been called
    auto nnz_per_row() const {
        // FIXME: assert(is_sorted_and_unique());
        std::vector<int> block_result;
        block_result.resize(nrows);
        assert(std::all_of(block_result.begin(), block_result.end(),
                           [](auto &&x) { return x == 0; }));
        for (auto &&t : entries) {
            assert(t.row() >= 0 && t.row() < block_result.size());
            ++block_result[t.row()];
        }
        std::vector<int> result;
        result.reserve(m * nrows);
        for (auto &&i : block_result) {
            for (size_type k = 0; k < m; ++k) {
                result.push_back(i * n);
            }
        }
        return result;
    }

    // not used yet, use of set_zero_if
    template <typename F>
    void erase_if(F &&condition) {
        // CHECK: if sorted can log search + break
        // is probably quicker to set_zero than remove elements
        std::erase_if(entries, std::forward<F>(condition));
    }

    template <typename F>
    void set_zero_if(F &&condition) {
        // CHECK: if sorted can log search + break
        // is probably quicker to set_zero than remove elements
        for (auto &&t : entries) {
            if (condition(t)) t.value_ref().setZero();
        }
    }

    template <typename F>
    void erase_if_contiguous(F &&condition) {
        // Erases all entries for which condition is true,
        // assuming they are stored as a contiguous chunk
        auto p = entries.begin();
        while (p != entries.end() && !condition(*p)) ++p;
        auto q = p;
        while (q != entries.end() && condition(*q)) ++q;
        assert(std::none_of(q, entries.end(), condition));
        entries.erase(p, q);
    }

    // FIXME: might be more efficient to append to buffer
    // std::vector<triplet> get_row(index_type i) const {
    //     std::vector<triplet> row{};
    //     row.reserve(30);  // Upper bound on the number of non zeros
    //     for (auto &&t : entries) {
    //         if (t.row() == i) {
    //             row.push_back(t);
    //         }
    //     }
    //     row.shrink_to_fit();
    //     return row;
    // }

    // std::vector<triplet> get_col(index_type j, const bool filter_zero=false)
    // const {
    //     std::vector<triplet> col{};
    //     col.reserve(30);  // FIXME: upper bound on the number of non zeros
    //     for (auto &&t : entries) {
    //         if (t.col() == j) {
    //             if(filter_zero && t.value_ref().isZero()) continue;
    //             col.push_back(t);
    //         }
    //     }
    //     col.shrink_to_fit();
    //     return col;
    // }

    // set_zero the blocks where condition is true, add identity block for
    // diag_indices. When l!=-1, done only for the block line l
    template <int l = -1>
        requires(l == -1 || (l >= 0 && l < m && l < n))
    void set_diagonal(auto const &condition, const auto &diag_indices,
                      auto value) {
        // keep track of diagonal blocks :
        // set diagonal block only once (because all
        // the values will be summed: additif contribution)
        // the size of identity_set is horible compared to what is necessary
        std::vector<bool> identity_set(nrows);
        entry_type diag_block = value * entry_type::Identity();
        for (auto &&t : entries) {
            // if the triplet is concerned
            if (condition(t)) {
                std::pair<index_type, index_type> rc{t.row(), t.col()};
                // find out if diagonal element
                auto p =
                    std::find(diag_indices.begin(), diag_indices.end(), rc);
                // if diag and not already "value" on the diagonal
                if (p != diag_indices.end() && !identity_set[t.row()]) {
                    // set the whole block to diagonal or modify only the
                    // line l
                    if constexpr (l == -1) {
                        t.value_ref() = diag_block;
                    } else {
                        t.value_ref().row(l) = diag_block.row(l);
                    }
                    identity_set[t.row()] = true;
                    continue;
                }
                // outside the diag or already set to "value" (additive
                // contribution)
                if constexpr (l == -1) {
                    t.value_ref().setZero();
                } else {
                    t.value_ref().row(l).setZero();
                }
            }
        }
        // add the diagonal block if missing
        for (auto &&[r, c] : diag_indices) {
            if (!identity_set[r]) {
                auto &ib = get_new(r, c);  // ib = value_ref
                // already init to zero
                // if the diagonal is in the matrix, add the diag block
                if (condition(entries.back())) {
                    if constexpr (l == -1) {
                        ib = diag_block;
                    } else {
                        ib(l, l) = value;
                    }
                }
            }
        }
    }

    // find the entry with row index r and col index c
    // suppose the entries are sorted and unique !
    auto rc_entry(index_type r, index_type c, int start_dist = 0) {
        auto start = entries.begin() + start_dist;
        auto end = entries.end();
        int dist = std::distance(start, end);
        while (dist >= 1) {
            auto middle = start + int(dist / 2);
            if ((*middle).row() == r) {
                if ((*middle).col() == c) {
                    return middle;
                } else if ((*middle).col() < c) {
                    start = middle;
                    // to avoid infinite loop
                    if (dist == 1) break;
                } else {
                    end = middle;
                }
            } else if ((*middle).row() < r) {
                start = middle;
                // to avoid infinite loop
                if (dist == 1) break;
            } else if ((*middle).row() > r) {
                end = middle;
            }
            dist = std::distance(start, end);
        }
        // entry not found
        return entries.end();
    }

    // careful, the algo is very costly
    bool is_symetric(const auto &eps) {
        /* Return true if the matrix is symetric (with accuracy eps) and false
           otherwise */
        static_assert((m == n) &&
                      "Only matrix of square blocks can possibly be symetric");
        assert((nrows == ncols) &&
               "Only square matrix can possibly be symetric");
        assert(is_sorted_and_unique() &&
               "Matrix must be sorted for the 'is_symetric' method to work !");
        // store the indices of the non diag coef already tested
        std::vector<std::set<index_type>> already_tested(nrows - 1);

        for (auto t = entries.begin(); t != entries.end(); t++) {
            if ((*t).row() == (*t).col()) {
                // check that diagonal blocks are symetric
                auto delta =
                    ((*t).value_ref().transpose() - (*t).value_ref()).norm();
                if (delta >= eps) return false;
            } else {
                // for a non-diagonal block t,
                // find the opposite block p if not already tested
                auto min_rc = std::min((*t).row(), (*t).col());
                auto max_rc = std::max((*t).row(), (*t).col());
                auto &&set_i = already_tested[min_rc];
                // couple (row, col) not already tested, if already tested,
                // check next entry
                if (set_i.find(max_rc) == set_i.end()) {
                    // todo: would like to pass t instead of dist
                    int dist = std::distance(entries.begin(), t);
                    auto p = rc_entry((*t).col(), (*t).row(), dist);
                    // symetric block found, check that p^T is equal to t
                    if (p != entries.end()) {
                        // values are sorted and unique so if one block is
                        // found, it is the only one with these (row, col) thus
                        // check this block is the symetry
                        auto delta =
                            ((*p).value_ref().transpose() - (*t).value_ref())
                                .norm();
                        if (delta >= eps) return false;
                        // store the info that (row, col) already tested
                        already_tested[min_rc].insert(max_rc);
                    }
                    // if there is no opposite block, t should be null
                    if (p == entries.end() && (*t).value_ref().norm() >= eps)
                        return false;
                }
            }
        }
        return true;
    }

    // FIXME: outside ?
    friend std::ostream &operator<<(std::ostream &os,
                                    const ElementaryBlockCOO &coo) {
        os << "COO matrix of size: (" << std::get<0>(coo.size()) << ", "
           << std::get<1>(coo.size()) << ")\n";
        os << "Entries set: " << coo.entries.size();
        os << std::endl;
        for (auto &&t : coo.entries) {
            os << t;
            os << std::endl;
        }
        return os;
    }
};

}  // namespace loaf
