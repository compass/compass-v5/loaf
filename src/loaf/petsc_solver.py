from packaging import version
import loaf

assert loaf.with_petsc
import petsc4py

assert version.parse(petsc4py.__version__) >= version.parse("3.19.1")

import sys

petsc4py.init(sys.argv)

from .preconditioners import CPRAMG, BlockJacobi
from loaf import fill_PETSc_Mat
from petsc4py import PETSc
from .solver import LinearSolverStatus, IterativeSolverSettings
import numpy as np
from .petsc_exceptions import IterativeSolverFailure, DirectSolverFailure


PETSc_version = PETSc.Sys.getVersion()
if PETSc_version[0] >= 3 and PETSc_version[1] > 19:
    _ksp_is_converged = lambda ksp: ksp.is_converged
else:
    _ksp_is_converged = lambda ksp: ksp.converged


class PetscLinearSystem:
    """
    A structure that holds and manages the linear system as
    Petsc objects
    """

    def __init__(self):
        # CHECKME: PETSc can improve performance by using knowledge
        # of the nonzero structure of the matrix.
        # See https://petsc.org/release//manualpages/Mat/MatCreateAIJ/
        self.initialized = False
        self.x = None
        self.A = None
        self.RHS = None

    def init(self, matrix):
        assert not self.initialized
        assert PETSc.COMM_WORLD.size == 1, "parallel version to be implemented"

        self.x = PETSc.Vec()
        self.A = PETSc.Mat()
        self.RHS = PETSc.Vec()

        nr, nc = matrix.full_size
        nnz = matrix.coo.nnz_per_row()
        self.A.createAIJ(size=(nr, nc), nnz=(nnz, 0))
        self.x.createMPI(nr)
        self.RHS.createMPI(nr)
        self.initialized = True

    @property
    def residual(self):

        """
        Residual norm ||b-A*x||
        """
        return (self.RHS - self.A * self.x).norm(PETSc.NormType.NORM_2)

    def fill(self, matrix):
        # FIXME: may be done several times use a boolean flag
        #        iscompressed (cf. Eigen)
        matrix.coo.sort_and_add_entries()
        if not self.initialized:
            self.init(matrix)
        fill_PETSc_Mat(self.A, matrix)

    def dump(self, filename=""):
        assert self.initialized
        dumper = PETSc.Viewer().createASCII(filename + "A.dat")
        self.A.view(dumper)
        dumper = PETSc.Viewer().createASCII(filename + "RHS.dat")
        self.RHS.view(dumper)


class PetscSolverBase:
    def __init__(self, settings=None, comm=PETSc.COMM_WORLD):
        self.initialized = False
        self.comm = comm
        self.settings = settings
        self.linear_system = PetscLinearSystem()
        self.ksp = None
        self.pc = None

    def init(self, matrix):
        assert not self.initialized
        assert not self.linear_system.initialized
        assert PETSc.COMM_WORLD.size == 1, "parallel version to be implemented"
        self.linear_system.init(matrix)
        self.ksp = PETSc.KSP().create(self.comm)
        self.ksp.setOperators(self.linear_system.A, self.linear_system.A)
        self.ksp.setFromOptions()
        self.initialized = True

    def set_up(self, matrix):
        if not self.initialized:
            matrix.coo.sort_and_add_entries()
            self.init(matrix)
        self.linear_system.fill(matrix)

    def solve(self, RHS):
        assert self.initialized
        # CHECKME: How much do the copies affect performance ?
        self.ksp.setConvergenceHistory()
        self.linear_system.RHS.getArray()[:] = RHS.as_array()
        self.ksp.solve(self.linear_system.RHS, self.linear_system.x)
        if self.ksp.reason < 0:  # Negative reason corresponds to convergence failure
            raise self.exception(self.ksp.reason, self.ksp.its)
        ret = RHS.get_twin()
        ret.as_array()[:] = self.linear_system.x.getArray()
        return ret, self.status

    def residual(self):
        assert self.initialized
        return self.linear_system.residual


class PetscSolver(PetscSolverBase):
    def __init__(self, activate_cpramg=True, **kwargs):
        super().__init__(**kwargs)
        self.activate_cpramg = activate_cpramg
        self.exception = IterativeSolverFailure

    def init(self, matrix):
        super().init(matrix)
        self.ksp.setType("gmres")
        self.ksp.setNormType(PETSc.KSP.NormType.UNPRECONDITIONED)
        settings = self.settings or IterativeSolverSettings()
        self.ksp.setTolerances(rtol=settings.tolerance, max_it=settings.maxiter)
        if self.activate_cpramg:
            self.ksp.setPC(CPRAMG(self.linear_system.A, matrix.block_size))
        else:
            self.ksp.setPC(BlockJacobi(self.linear_system.A))

    @property
    def status(self):
        assert self.initialized
        return LinearSolverStatus(
            niterations=self.ksp.its,
            solve_success=self.ksp.reason > 0,
            maxiter_reached=self.ksp.its == self.ksp.max_it,
            convergence_reached=_ksp_is_converged(self.ksp),
            residual_history=self.ksp.history,
        )


class PetscDirectSolver(PetscSolverBase):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def exception(self, reason, its):
        return DirectSolverFailure(reason)

    def init(self, matrix):
        super().init(matrix)
        self.ksp.setType("preonly")
        self.ksp.pc.setType("lu")
        self.ksp.pc.setFactorSolverType("superlu")

    @property
    def status(self):
        return LinearSolverStatus(
            niterations=1,
            solve_success=self.ksp.reason > 0,
            maxiter_reached=None,
            convergence_reached=None,
            residual_history=[],
        )
