#pragma once

#include <Eigen/Dense>
#include <array>
#include <ranges>
#include <tuple>
#include <type_traits>

#include "loaf/types.h"

using std::views::cartesian_product;
using std::views::iota;
using std::views::transform;
using std::views::zip;

namespace loaf {

// FIXME: is this relevant ? -> real_type ?
using value_type = double;

template <std::size_t n>
auto row_major_if_possible = []() {
    if constexpr (n == 1)
        return Eigen::ColMajor;
    else
        return Eigen::RowMajor;
};

// FIXME: why row_major_if_possible (difference Vector/Matrix ?)
// FIXME: dissociate position and value to have test on position only
template <std::size_t m, std::size_t n>
using ElementaryBlock =
    Eigen::Matrix<value_type, m, n, row_major_if_possible<n>()>;

template <std::size_t m>
using ElementaryColBlock = ElementaryBlock<m, 1>;

template <typename Entry>
struct COO_triplet {
    using entry_type = Entry;
    index_type _row;
    index_type _column;
    entry_type _entry;

    // The order should in/on a matrix_location structure

    bool operator==(const COO_triplet &other) const {
        return (_row == other._row && _column == other._column);
    }
    bool operator>=(const COO_triplet &other) const {
        if (_row == other._row) {
            return (_column >= other._column);
        }
        return _row >= other._row;
    }
    bool operator<=(const COO_triplet &other) const {
        if (_row == other._row) {
            return (_column <= other._column);
        }
        return _row <= other._row;
    }
    bool operator<(const COO_triplet &other) const {
        if (_row == other._row) {
            return _column < other._column;
        }
        return _row < other._row;
    }

    // The following methods are defined for
    // compatibility with Eigen::setFromTriplets;
    // FIXME: Eigen only defines value which is more intuitive
    entry_type &value_ref() { return _entry; }
    const entry_type &value() const { return _entry; }
    index_type row() const { return _row; }
    index_type col() const { return _column; }
    friend std::ostream &operator<<(std::ostream &os,
                                    const COO_triplet<entry_type> &t) {
        os << "(" << t._row << ", " << t._column << "): ";
        if constexpr (!std::is_scalar_v<entry_type>) {
            os << "\n";
            os << t.value();
        } else {
            os << t.value();
        }
        return os;
    }

    auto split() const
        requires(!std::is_scalar_v<entry_type>)
    {
        // Returns a range view of the individual matrix entries
        // after splitting the block
        using EBlock = entry_type;
        constexpr auto m = EBlock::RowsAtCompileTime;
        constexpr auto n = EBlock::ColsAtCompileTime;
        constexpr auto storage_order = EBlock::Options;
        auto I = row() * m, J = col() * n;

        return zip(cartesian_product(iota(I, I + m), iota(J, J + n)),
                   value().template reshaped<storage_order>());
    }

    auto split() const
        requires(std::is_scalar_v<entry_type>)
    {
        return std::ranges::single_view{
            std::make_tuple(std::make_tuple(row(), col()), value())};
    }
};

}  // namespace loaf
