import re
from pathlib import Path
import numpy as np
from scipy.sparse import csr_matrix


def petsc2csr(path, filter_out_zeros=True, parse_petsc_header=True):
    path = Path(path)
    assert path.exists()
    matcol = []
    matdat = []
    with path.open() as f:
        line = f.readline().strip()
        if parse_petsc_header:
            match = re.match("Mat Object: (\d+) MPI processes", line)
            # 1 proc only
            assert match and int(match.groups()[0]) == 1
        f.readline()
        column_extraction = re.compile("row (\d+): (.*)")
        data_extraction = re.compile("\((\d+), (.+)\)")
        line = f.readline().strip()
        while line:
            match = column_extraction.match(line)
            groups = match.groups()
            row = int(groups[0])
            data = [s.strip() for s in groups[1].strip().split("  ")]
            cols = []
            nz = []
            for s in data:
                groups = data_extraction.match(s).groups()
                cols.append(int(groups[0]))
                try:
                    nz.append(float(groups[1]))
                except ValueError:
                    print("\nconversion error on row %d:\n%s\n" % (row, data))
                    raise
            assert len(matcol) == len(matdat) == row
            cols = np.array(cols)
            nz = np.array(nz)
            assert not np.any(np.isnan(nz))
            if filter_out_zeros:
                keep = nz != 0
            else:
                keep = np.ones(nz.shape[0], dtype="b")
            matcol.append(cols[keep])
            matdat.append(np.array(nz[keep]))
            line = f.readline().strip()

    lengths = np.array([a.shape[0] for a in matcol])
    assert np.all(np.array([a.shape[0] for a in matdat]) == lengths)
    assert len(matdat) == row + 1

    return csr_matrix(
        (
            np.hstack(matdat),
            np.hstack(matcol),
            np.hstack([[0], np.cumsum(lengths)]),
        ),
        shape=(row + 1, row + 1),
    )
