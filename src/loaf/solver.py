#
# This file is part of ComPASS.
#
# ComPASS is free software: you can redistribute it and/or modify it under both the terms
# of the GNU General Public License version 3 (https://www.gnu.org/licenses/gpl.html),
# and the CeCILL License Agreement version 2.1 (http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html).
#

from collections import namedtuple
from compass_utils.mpi import master_print
from typing import NamedTuple


class LinearSolverFailure(Exception):
    def __init__(self, reason):
        self.reason = reason

    def __str__(self):
        return f"Linear solver failed with reason : {self.reason}"


class LinearSolverStatus(NamedTuple):
    solve_success: bool = None
    maxiter_reached: bool = None
    convergence_reached: bool = None
    niterations: int = 0
    residual_history: list = []
    message: str = ""


class IterativeSolverSettings(NamedTuple):
    method: str = "gmres"
    # 1e-6 in v4 when direct=False (redefined in linear_solver in factory.py)
    tolerance: float = 1.0e-6
    maxiter: int = 150
    restart_size: int = None


# class LinearSolver:
#     """
#     A base structure to hold the common parameters of ComPASS linear solvers
#     """

#     def __init__(self, linear_system):
#         """
#         :param linear_system: the linear system structure
#         """
#         self.failures = 0
#         self.linear_system = linear_system
#         # if compass_config.get("lsolver.view"):
#         #     master_print(self)
#         # self.failure_callbacks = ()

#     def write_history(self, basename=""):
#         with open(f"{basename}/solver_log.txt", "w") as f:
#             f.write("Linear solver used has no logging method implemented")

#     def __str__(self):
#         return "LinearSolver object view:"


# class DirectSolver(LinearSolver):
#     def __init__(self, linear_system):
#         super().__init__(linear_system)

#     # Just to have common treatments with IterativeSolver
#     @property
#     def residual_history(self):
#         return tuple()

#     def __str__(self):
#         return f"{super().__str__()}\n   Direct"


# class IterativeSolver(LinearSolver):
#     def __init__(self, linear_system, settings):
#         """
#         :param settings: An IterativeSolverSettings object containing the wanted parameters for iterative solving
#         """
#         self.number_of_successful_iterations = 0
#         self.number_of_unsuccessful_iterations = 0
#         self.nit = 0
#         self.my_settings = settings
#         self.residual_history = []
#         super().__init__(linear_system)

#     def __str__(self):
#         return f"{super().__str__()}\n   Iterative"
