#pragma once

#include "elementary_block.h"

namespace loaf {
/** Linear Block Elimination

    usefull for elimination based on local Schur complement

    While trying to solve A*u=b under local linear constaints C(s)*u(s)=r(s),
    you end up willing to solve A'*u'=b'.
    Block structure is shared beetwen A and A' but block sizes are different.
    You decide that u and u' are linked by a local linear transformation:
      u(s) = Y(s) * u'(s) - z(s)
    thus the linear system becomes
      A*Y * u' = b + A*z
    where Y is block diagonal.

    Having A' full rank requires:
      C(s) * Y(s) = 0
      C(s) * z(s) = r(s)
    Several choices for (Y(s), z(s)) often remain possible.

    Here is a sketch of such usage where the elimination strategy is defined by
    the class ClosureElimination:
    ```
    auto closure = ClosureElimination(laws_by_rocktype, other_params)
    icus::make_field<ClosureElimination::eliminator_t>(I) eliminators ;
    // eliminator_t -> LinearBlockEliminator<N_bilan, N_naturel>;
    ...
    closure.update_local_elimination(eliminators(s), contexts(s), rocktypes(s),
   states(s));
    // do things like:
    // eliminator.mat_factor(all, all) = f_J(context, rocktype, state) ;
    // eliminator.rhs_factor(all, all) = f_b(context, rocktype, state) ;
    ...
    Jac(i,s) += eliminators(s).mat_incr(Jis) ;
    residual(i) += eliminators(s).rhs_incr(Jis) ;
    ...
    // U = Jac.inv * residual ;
    Ufull(s) = eliminators(s).reconstruct(U(s)) ;
    ```
 */
template <std::size_t m, std::size_t n>
struct LinearBlockEliminator {
    template <std::size_t d1, std::size_t d2>
    using block_t = loaf::ElementaryBlock<d1, d2>;
    block_t<n, m> mat_factor;  // yes, transpose
    block_t<n, 1> rhs_factor;
    // no need for update() because public members and no fancy constructor
    block_t<m, m> mat_incr(const block_t<m, n>& J) { return J * mat_factor; }
    block_t<m, 1> rhs_incr(const block_t<m, n>& J) { return J * rhs_factor; }
    block_t<n, 1> reconstruct(const block_t<m, 1>& U) {
        return mat_factor * U - rhs_factor;
    };
};
}  // namespace loaf
