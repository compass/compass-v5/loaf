#pragma once

#include <tuple>

#include "icus/ISet.h"
#include "loaf/block_matrix.h"
// #include "loaf/matrix.h"
#include "loaf/vector.h"

namespace loaf {
template <size_type block_size>
auto make_block_linear_system(const icus::ISet sites) {
    return std::make_pair(OnePhysicsBlockCOO<block_size>(sites),
                          OnePhysicsVector<block_size>(sites));
}
}  // namespace loaf
